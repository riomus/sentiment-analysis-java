package pl.riomus.spring.rest.coniguration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.annotation.Resource;
import javax.sql.DataSource;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
@Configuration
@PropertySource("classpath:db.properties")
public class DataSourceConfiguration {
    private static final String PROPERTY_NAME_DATABASE_DRIVER = "db.driver";
    private static final String PROPERTY_NAME_DATABASE_PASSWORD_DEFAULT = "db.default.password";
    private static final String PROPERTY_NAME_DATABASE_URL_DEFAULT = "db.default.url";
    private static final String PROPERTY_NAME_DATABASE_USERNAME_DEFAULT = "db.default.username";
    private static final String PROPERTY_NAME_DATABASE_PASSWORD = "db.password";
    private static final String PROPERTY_NAME_DATABASE_URL = "db.url";
    private static final String PROPERTY_NAME_DATABASE_USERNAME = "db.username";

    @Resource
    private Environment env;

    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(env.getRequiredProperty(PROPERTY_NAME_DATABASE_DRIVER));
        dataSource.setUrl(System.getProperty(PROPERTY_NAME_DATABASE_URL, env.getRequiredProperty(PROPERTY_NAME_DATABASE_URL_DEFAULT)));
        dataSource.setUsername(System.getProperty(PROPERTY_NAME_DATABASE_USERNAME, env.getRequiredProperty(PROPERTY_NAME_DATABASE_USERNAME_DEFAULT)));
        dataSource.setPassword(System.getProperty(PROPERTY_NAME_DATABASE_PASSWORD, env.getRequiredProperty(PROPERTY_NAME_DATABASE_PASSWORD_DEFAULT)));
        return dataSource;
    }
}
