package pl.riomus.wordcoexistence.redis.vectorizer;

import com.google.gson.Gson;
import pl.riomus.nlp.api.wordcoexistence.CoexistenceEntry;
import pl.riomus.nlp.api.wordcoexistence.CoexistencyEntryProvider;
import redis.clients.jedis.Jedis;

import java.util.Optional;
import java.util.Set;

/**
 * Created by Roman Bartusiak (roman.bartusiak@nsn.com)
 */
public class RedisCoexistencyEntryClient implements CoexistencyEntryProvider{

    public static final int JEDIS_TIMEOUT = 10000;
    private final Gson gson;
    private Jedis jedis;

    public RedisCoexistencyEntryClient(String redisAddress, int port) {
        jedis=new Jedis(redisAddress,port, JEDIS_TIMEOUT);
        gson=new Gson();
    }

    @Override
    public Optional<CoexistenceEntry> getCoexistencyEntry(String word) {
        CoexistenceEntry entry=null;
        if(word!=null) {
            String jsonData = jedis.get(word);
            if (jsonData != null) {
                entry = gson.fromJson(jsonData, CoexistenceEntry.class);
            }
        }
        return Optional.ofNullable(entry);
    }

    public Set<String> getAllWords(){
        return jedis.keys("*");
    }

    @Override
    public int getCoexsistenctVectorSize() {
        return gson.fromJson(jedis.get(jedis.randomKey()),CoexistenceEntry.class).getVectorSize();
    }

}
