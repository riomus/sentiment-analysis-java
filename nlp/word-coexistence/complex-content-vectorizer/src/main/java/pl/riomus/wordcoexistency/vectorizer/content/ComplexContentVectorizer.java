package pl.riomus.wordcoexistency.vectorizer.content;

import pl.riomus.nlp.api.ContentTagger;
import pl.riomus.nlp.api.contentvectorizer.ContentVectorizer;
import pl.riomus.nlp.api.model.TaggedContent;
import pl.riomus.nlp.api.model.ccl.Chunk;
import pl.riomus.nlp.api.model.ccl.Sentence;
import pl.riomus.nlp.api.model.ccl.Token;
import pl.riomus.nlp.api.wordcoexistence.WordVectorizer;

import java.util.*;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
public class ComplexContentVectorizer implements ContentVectorizer{
    private ContentTagger contentTagger;
    private WordVectorizer wordVectorizer;

    public ComplexContentVectorizer(ContentTagger contentTagger, WordVectorizer wordVectorizer) {
        this.contentTagger = contentTagger;
        this.wordVectorizer = wordVectorizer;
    }

    @Override
    public double[][] vectorize(String content) {

        Map<String,Optional<String>> words=new HashMap<>();
        TaggedContent taggedContent =  contentTagger.tag(content);
        if(taggedContent!=null&&taggedContent.getContent()!=null&&taggedContent.getContent().getChunks()!=null)
            for (Chunk chunk : taggedContent.getContent().getChunks()) {
                for (Sentence sentence : chunk.getSentences()) {
                    for (Token token : sentence.getTokens()) {
                        String base = token.getLexicalUnit().getBase().trim();
                        words.put(token.getOrth(),Optional.of(base));
                    }

                }

            }
        List<double[]> out=new ArrayList<>();
        for (String s : words.keySet()) {
            String toVectorizeWord;
            if(words.get(s).isPresent()){
                toVectorizeWord=words.get(s).get();
            }else{
                toVectorizeWord=s;
            }
            Optional<double[]> vectorizedWord=wordVectorizer.vectorize(toVectorizeWord);
            if(!vectorizedWord.isPresent()){
                vectorizedWord=wordVectorizer.vectorize(s);
            }
            double[] vector;
            if(vectorizedWord.isPresent()){
                vector=vectorizedWord.get();
            }else{
                vector=wordVectorizer.getUnknownVector();
            }
            out.add(vector);
        }

        double[][] outArray=new double[out.size()][];
        for(int i=0;i<out.size();i++){
            outArray[i]=out.get(i);
        }
        return outArray;
    }
}
