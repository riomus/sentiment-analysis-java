package pl.riomus.nlp.wordcoexistence.vectorizer;

import pl.riomus.nlp.api.wordcoexistence.WordVectorizer;
import pl.riomus.nlp.wordcoexistence.data.DataFiles;
import pl.riomus.nlp.api.wordcoexistence.CoexistenceEntry;
import pl.riomus.nlp.wordcoexistence.loader.MatrixLoader;
import pl.riomus.nlp.api.wordcoexistence.ValueEntry;
import pl.riomus.wordcoexistence.common.ToWordVectorTransformator;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Created by Roman Bartusiak (roman.bartusiak@nsn.com)
 */
public class InMemoryWordVectorizer implements WordVectorizer {
      private Map<String, CoexistenceEntry> vectorCache;
    private int vectorLength;
    private ToWordVectorTransformator toWordVectorTransformator;

    public InMemoryWordVectorizer(){
        this(new MatrixLoader(DataFiles.MATRIX_FILE,DataFiles.LABEL_FILE));
    }

    public InMemoryWordVectorizer(MatrixLoader matrixLoader) {
        initialize(matrixLoader);
    }

    private void initialize(MatrixLoader matrixLoader) {
        vectorCache = new HashMap<>();
        while(matrixLoader.hasNext()){
            CoexistenceEntry entry = matrixLoader.next();
            String[] splitedWord=entry.getWord().split(":");
            vectorLength=entry.getVectorSize();
            vectorCache.put(splitedWord[splitedWord.length - 1], entry);
        }
        toWordVectorTransformator = new ToWordVectorTransformator(vectorLength);
    }

    @Override
    public Optional<double[]> vectorize(String word) {
       CoexistenceEntry coexistenceEntry = vectorCache.get(word);
       return toWordVectorTransformator.getVector(Optional.ofNullable(coexistenceEntry));
    }

    @Override
    public double[] getUnknownVector() {
        return toWordVectorTransformator.getUnknownVector();
    }
}
