package pl.riomus.nlp.wordcoexistence.loader;

/**
 * Created by Roman Bartusiak (roman.bartusiak@nsn.com)
 */
public class MatrixLoadException extends RuntimeException {
    public MatrixLoadException(Exception e) {
        super(e);
    }
}
