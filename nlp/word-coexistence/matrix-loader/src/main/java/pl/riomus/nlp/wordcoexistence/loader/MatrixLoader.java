package pl.riomus.nlp.wordcoexistence.loader;

import pl.riomus.nlp.api.wordcoexistence.CoexistenceEntry;
import pl.riomus.nlp.api.wordcoexistence.ValueEntry;

import java.io.Closeable;
import java.io.File;
import java.io.FileReader;
import java.util.*;

/**
 * Created by Roman Bartusiak (roman.bartusiak@nsn.com)
 */
public class MatrixLoader implements Iterator<CoexistenceEntry>,Closeable{
    private Scanner matrixFileScanner;
    private Scanner labelFileScanner;
    private int vectorSize;

    public MatrixLoader(String matrixFileName, String labelFileName) {
        initialize(matrixFileName,labelFileName);
    }

    private void initialize(String matrixFileName, String labelFileName) {
        try{
            tryToInitialize(matrixFileName,labelFileName);
        }
        catch(Exception e){
            throw new MatrixLoadException(e);
        }
    }

    private void tryToInitialize(String matrixFileName, String labelFileName) throws Exception{
        File matrixFile=new File(matrixFileName);
        File labelFile=new File(labelFileName);
        matrixFileScanner = new Scanner(new FileReader(matrixFile));
        labelFileScanner = new Scanner(new FileReader(labelFile));
        String headerString = matrixFileScanner.nextLine();
        Scanner headerStringScanner=new Scanner(headerString);
        vectorSize = headerStringScanner.nextInt();
        headerStringScanner.close();
    }


    @Override
    public boolean hasNext() {
        return matrixFileScanner.hasNextLine();
    }

    @Override
    public CoexistenceEntry next() {
        List<ValueEntry> values=new ArrayList<>();
        String vectorLine = matrixFileScanner.nextLine();
        String word=labelFileScanner.nextLine();
        Scanner vectorScanner=new Scanner(vectorLine);
        vectorScanner.useLocale(Locale.ENGLISH);
        while(vectorScanner.hasNextInt()){
            int vectorIndex=vectorScanner.nextInt();
            double vectorValue=vectorScanner.nextDouble();
           values.add(new ValueEntry(vectorIndex,vectorValue));
        }
        return new CoexistenceEntry(word, values,vectorSize);
    }

    @Override
    public void close(){
        matrixFileScanner.close();
        labelFileScanner.close();
    }
}
