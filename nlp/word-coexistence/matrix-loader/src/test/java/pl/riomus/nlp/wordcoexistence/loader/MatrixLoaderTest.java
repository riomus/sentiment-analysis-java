package pl.riomus.nlp.wordcoexistence.loader;

import org.junit.Before;
import org.junit.Test;
import pl.riomus.nlp.api.wordcoexistence.CoexistenceEntry;
import pl.riomus.nlp.api.wordcoexistence.ValueEntry;

import java.util.List;

import static org.junit.Assert.*;

public class MatrixLoaderTest {

    private MatrixLoader matrixLoader;

    @Before
    public void setUp() {
        String matrixFile = MatrixLoaderTest.class.getClassLoader().getResource("matrix_test.mat").getFile();
        String labelsFile = MatrixLoaderTest.class.getClassLoader().getResource("matrix_labels_test.mat").getFile();
        matrixLoader = new MatrixLoader(matrixFile, labelsFile);
    }
    @Test
    public void shouldLoadSimpleMatFile() throws Exception {
            assertTrue(matrixLoader.hasNext());
            CoexistenceEntry entry = matrixLoader.next();
            assertEquals("word", entry.getWord());
        List<ValueEntry> values = entry.getValues();
        assertEquals(2,values.get(0).getIndex());
        assertEquals(2.36, values.get(0).getValue(), 0);
        assertEquals(3,values.get(1).getIndex());
        assertEquals(0.25,values.get(1).getValue(),0);
            assertFalse(matrixLoader.hasNext());
            matrixLoader.close();
    }
}