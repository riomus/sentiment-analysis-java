package pl.riomus.wordcoexistence.common;

import pl.riomus.nlp.api.wordcoexistence.CoexistenceEntry;
import pl.riomus.nlp.api.wordcoexistence.ValueEntry;

import java.util.Arrays;
import java.util.Optional;
import java.util.function.Function;

/**
 * Created by Roman Bartusiak (roman.bartusiak@nsn.com)
 */
public class ToWordVectorTransformator implements Function<CoexistenceEntry, double[]> {

    private int vectorLength;

    public ToWordVectorTransformator(int vectorLength) {
        this.vectorLength = vectorLength;
    }

    public Optional<double[]> getVector(Optional<CoexistenceEntry> coexistenceEntry) {
        return coexistenceEntry.map(this);
    }

    public double[] getUnknownVector() {
        double[] vector = new double[vectorLength];
        Arrays.fill(vector,0.);
        return vector;
    }

    @Override
    public double[] apply(CoexistenceEntry coexistenceEntry) {
        double[] vector = new double[vectorLength];
        Arrays.fill(vector, 0.);
        for (ValueEntry valueEntry : coexistenceEntry.getValues()) {
            vector[valueEntry.getIndex()] = valueEntry.getValue();
        }
        return vector;
    }
}
