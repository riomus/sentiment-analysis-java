package pl.riomus.nlp.wordcoexistence.loader;

import com.google.gson.Gson;
import pl.riomus.nlp.api.wordcoexistence.CoexistenceEntry;
import pl.riomus.nlp.wordcoexistence.data.DataFiles;
import redis.clients.jedis.Jedis;

import java.util.logging.Logger;

/**
 * Created by Roman Bartusiak (roman.bartusiak@nsn.com)
 */
public class RedisLoader {
    public static final int JEDIS_TIMEOUT = 100000;
    private String redisAddress;
    private Integer port;
    private static final Logger LOG= Logger.getLogger(RedisLoader.class.getCanonicalName());

    public RedisLoader(String redisAddress,Integer port) {
        this.redisAddress = redisAddress;
        this.port = port;
    }
    
    public void loadData(){
        Jedis jedis = new Jedis(redisAddress,port, JEDIS_TIMEOUT);
        jedis.connect();

        Gson gson=new Gson();
        MatrixLoader matrixLoader = new MatrixLoader(DataFiles.MATRIX_FILE, DataFiles.LABEL_FILE);
        while(matrixLoader.hasNext()){
            CoexistenceEntry entry = matrixLoader.next();
            String word = getWord(entry.getWord());
            LOG.info("Persisted word: " + word);
            jedis.set(word, gson.toJson(entry));
            System.gc();
        }
        jedis.close();
        matrixLoader.close();
    }

    private String getWord(String word) {
        String[] splitedWord = word.split(":");
        return splitedWord[splitedWord.length-1];
    }

    public static void main(String[] args) {
        RedisLoader redisLoader = new RedisLoader("localhost",6379);
        redisLoader.loadData();
    }
}
