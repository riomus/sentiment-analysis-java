package pl.riomus.wordcoexistence.redis.vectorizer;

import pl.riomus.nlp.api.wordcoexistence.CoexistencyEntryProvider;
import pl.riomus.nlp.api.wordcoexistence.WordVectorizer;
import pl.riomus.wordcoexistence.common.ToWordVectorTransformator;

import java.util.Optional;

/**
 * Created by Roman Bartusiak (roman.bartusiak@nsn.com)
 */
public class CoexistencyEntryBasedVectorizer implements WordVectorizer {

    private final ToWordVectorTransformator toWordVectorTransformator;
    private CoexistencyEntryProvider coexistencyEntryProvider;

    public CoexistencyEntryBasedVectorizer(CoexistencyEntryProvider coexistencyEntryProvider,ToWordVectorTransformator toWordVectorTransformator) {
        this.coexistencyEntryProvider = coexistencyEntryProvider;
        this.toWordVectorTransformator = toWordVectorTransformator;
    }

    @Override
    public Optional<double[]> vectorize(String word) {
        return toWordVectorTransformator.getVector(coexistencyEntryProvider.getCoexistencyEntry(word));
    }

    @Override
    public double[] getUnknownVector() {
        return toWordVectorTransformator.getUnknownVector();
    }
}
