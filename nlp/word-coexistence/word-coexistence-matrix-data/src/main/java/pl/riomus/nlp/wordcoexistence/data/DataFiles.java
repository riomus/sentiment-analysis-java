package pl.riomus.nlp.wordcoexistence.data;

/**
 * Created by Roman Bartusiak (roman.bartusiak@nsn.com)
 */
public interface DataFiles {
    public static final String MATRIX_FILE=DataFiles.class.getClassLoader().getResource("matrix.mat").getFile();
    public static final String LABEL_FILE=DataFiles.class.getClassLoader().getResource("labels.clabel").getFile();

}
