package pl.riomus.wordcoexistence.scaler;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import la.matrix.Matrix;
import org.junit.Ignore;
import org.junit.Test;
import pl.riomus.nlp.api.wordcoexistence.CoexistenceEntry;
import pl.riomus.wordcoexistence.redis.vectorizer.RedisCoexistencyEntryClient;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static la.io.IO.loadMatrix;
import static la.io.IO.saveMatrix;
import static ml.utils.Matlab.l2Distance;

public class MDSCoexistencyScalerTest {

    public static final String OUTPUT_DIRECTORY = "/home/riomus/github/sentiment-analysis/java/nlp/word-coexistence/laml-coexistency-entry-scaler/src/main/resources/";
    public static final String LABELS_OUT = OUTPUT_DIRECTORY + "/rowLabels.json";
    public static final String MATRIX_OUT = OUTPUT_DIRECTORY + "/matrix.mat";
    public static final String DISTANCE_OUT = OUTPUT_DIRECTORY + "/distanceMatrix.mat";

    @Test
    @Ignore
    public void createMatrixAndWordLabels() throws Exception {
        RedisCoexistencyEntryClient redisCoexistencyEntryClient = new RedisCoexistencyEntryClient("localhost", 6379);
        Set<String> allWords = redisCoexistencyEntryClient.getAllWords();


        List<CoexistenceEntry> allCoexistencyEntries = allWords.stream().map(w -> redisCoexistencyEntryClient.getCoexistencyEntry(w).get()).collect(Collectors.toList());

        MatrixCreator matrixCreator = new MatrixCreator();
        Matrix wordMatrix = matrixCreator.createMatrix(allCoexistencyEntries);

        saveMatrix(wordMatrix, MATRIX_OUT);
        saveWordLabels(new ArrayList<>(allWords),LABELS_OUT);
    }

    @Test
    @Ignore
    public void createDistanceMatrix() throws Exception {
        Matrix wordMatrix=loadMatrix(MATRIX_OUT);
        Matrix distanceMatrix=l2Distance(wordMatrix,wordMatrix);
        saveMatrix(distanceMatrix,DISTANCE_OUT);
    }

    @Test
    @Ignore
    public void loadMatrixAndCreateScaledCoexistencyEntries() throws Exception {
        Matrix distanceMatrix=loadMatrix(DISTANCE_OUT);
        List<String> wordLabels=loadWordLabels(LABELS_OUT);
        MDSCoexistencyScaler mdsCoexistencyScaler = new MDSCoexistencyScaler(new MatrixCreator(), new CoexistencyEntryListCreator());
        mdsCoexistencyScaler.scaleToCoexistenceEntriesUsingDistanceMatrix(5, wordLabels, distanceMatrix);
    }

    private List<String> loadWordLabels(String labelsOut) {
        try {
            Gson gson = new Gson();
            BufferedReader bufferedReader = new BufferedReader(new FileReader(labelsOut));
            return gson.fromJson(bufferedReader,new TypeToken<List<String>>(){}.getType());
        } catch (FileNotFoundException e) {
            throw new RuntimeException("Failed to load row labels!",e);
        }
    }

    private void saveWordLabels(List<String> rowLabels,String out) {
        try{
            Gson gson=new Gson();
            FileWriter fileWriter = new FileWriter(out);
            fileWriter.write(gson.toJson(rowLabels));
            fileWriter.close();
        } catch (IOException e) {
            throw new RuntimeException("Failed to save row labels!",e);
        }
    }
}