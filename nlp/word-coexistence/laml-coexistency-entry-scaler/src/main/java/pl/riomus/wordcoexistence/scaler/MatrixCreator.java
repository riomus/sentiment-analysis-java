package pl.riomus.wordcoexistence.scaler;

import la.matrix.Matrix;
import la.matrix.SparseMatrix;
import la.vector.Vector;
import ml.utils.Pair;
import pl.riomus.nlp.api.wordcoexistence.CoexistenceEntry;
import pl.riomus.nlp.api.wordcoexistence.ValueEntry;

import java.util.List;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
public class MatrixCreator {

    public Matrix createMatrix(List<CoexistenceEntry> coexistenceEntryList){
     return SparseMatrix.createSparseMatrix(convertToValuesMap(coexistenceEntryList), coexistenceEntryList.size(), coexistenceEntryList.get(0).getVectorSize());
    }




    private TreeMap<Pair<Integer, Integer>, Double> convertToValuesMap(List<CoexistenceEntry> input) {
        return IntStream.range(0, input.size()).mapToObj(index -> convertToValuesMap(index, input.get(index)))
                .reduce(new TreeMap<>(), (m1, m2) -> {
                    m1.putAll(m2);
                    return m1;
                });
    }

    private TreeMap<Pair<Integer, Integer>, Double> convertToValuesMap(int index, CoexistenceEntry coexistenceEntry) {
        TreeMap<Pair<Integer, Integer>, Double> out = new TreeMap<>();
        coexistenceEntry.getValues().forEach(valueEntry -> out.put(new Pair(index, valueEntry.getIndex()), valueEntry.getValue()));
        return out;
    }
}
