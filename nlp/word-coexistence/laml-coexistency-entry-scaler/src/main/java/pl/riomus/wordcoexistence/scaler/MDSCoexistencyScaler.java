package pl.riomus.wordcoexistence.scaler;

import la.matrix.Matrix;
import ml.subspace.MDS;
import ml.utils.Matlab;
import pl.riomus.nlp.api.wordcoexistence.CoexistenceEntry;
import pl.riomus.nlp.api.wordcoexistence.CoexistencyEntryScaler;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
public class MDSCoexistencyScaler implements CoexistencyEntryScaler {
    private MatrixCreator matrixCreator;
    private CoexistencyEntryListCreator coexistencyEntryListCreator;

    public MDSCoexistencyScaler(MatrixCreator matrixCreator, CoexistencyEntryListCreator coexistencyEntryListCreator) {
        this.matrixCreator = matrixCreator;
        this.coexistencyEntryListCreator = coexistencyEntryListCreator;
    }

    @Override
    public List<CoexistenceEntry> scaleCoexistenceEntries(List<CoexistenceEntry> input, int outputDimension) {
        Matrix wordMatrix=matrixCreator.createMatrix(input);
        List<String> rowsLabels = input.stream().map(c -> c.getWord()).collect(Collectors.toList());
        return scaleToCoexistenceEntries(outputDimension, wordMatrix, rowsLabels);
    }

    public List<CoexistenceEntry> scaleToCoexistenceEntries(int outputDimension, Matrix wordMatrix, List<String> rowsLabels) {
        Matrix distanceMatrix = Matlab.l2Distance(wordMatrix, wordMatrix);
        return scaleToCoexistenceEntriesUsingDistanceMatrix(outputDimension, rowsLabels, distanceMatrix);
    }

    public List<CoexistenceEntry> scaleToCoexistenceEntriesUsingDistanceMatrix(int outputDimension, List<String> rowsLabels, Matrix distanceMatrix) {
        Matrix scaled = MDS.run(distanceMatrix, outputDimension);
        return coexistencyEntryListCreator.convertToCoexistencyEntryList(scaled, rowsLabels, outputDimension);
    }


}
