package pl.riomus.wordcoexistence.scaler;

import la.matrix.Matrix;
import la.vector.Vector;
import pl.riomus.nlp.api.wordcoexistence.CoexistenceEntry;
import pl.riomus.nlp.api.wordcoexistence.ValueEntry;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
public class CoexistencyEntryListCreator {
    public List<CoexistenceEntry> convertToCoexistencyEntryList(Matrix scaled, List<String> rowLabels, int outputDimension) {
        return IntStream.range(0, rowLabels.size()).mapToObj(index ->
                convertToCoexistenceEntry(scaled.getRowVector(index), rowLabels.get(index), outputDimension)).collect(Collectors.toList());
    }

    private CoexistenceEntry convertToCoexistenceEntry(Vector rowVector, String word, int outputDimension) {
        List<ValueEntry> collect = IntStream.range(0, rowVector.getDim())
                .filter(i -> rowVector.get(i) != 0.)
                .mapToObj(i -> new ValueEntry(i, rowVector.get(i)))
                .collect(Collectors.toList());
        return new CoexistenceEntry(word, collect, outputDimension);
    }
}
