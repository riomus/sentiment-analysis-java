package pl.riomus.sentimentanalysis.service.producers;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.riomus.nlp.api.wordcoexistence.WordVectorizer;
import pl.riomus.nlp.wcrft2.Wcrft2ServiceClient;
import pl.riomus.wordcoexistency.vectorizer.content.ComplexContentVectorizer;

import javax.inject.Inject;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
@Configuration
public class ContentVectorizerProducer {

    @Bean
    @Inject
    public ComplexContentVectorizer createContentVetorizer(Wcrft2ServiceClient contentTagger,WordVectorizer wordVectorizer){
        return new ComplexContentVectorizer(contentTagger,wordVectorizer);
    }
}
