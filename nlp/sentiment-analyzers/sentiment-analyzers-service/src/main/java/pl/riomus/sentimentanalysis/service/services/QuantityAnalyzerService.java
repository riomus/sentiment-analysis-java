package pl.riomus.sentimentanalysis.service.services;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import pl.riomus.nlp.api.model.SentimentAnalyzeResult;
import pl.riomus.nlp.api.sentimentanalyzers.SentimentAnalyzer;
import pl.riomus.sentimentanalysis.quantity.QuantitySentimentAnalyzer;

import javax.inject.Inject;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
@RestController
public class QuantityAnalyzerService {

    private SentimentAnalyzer quantitySentimentAnalyzer;

    @Inject
    public QuantityAnalyzerService(QuantitySentimentAnalyzer quantitySentimentAnalyzer) {
        this.quantitySentimentAnalyzer = quantitySentimentAnalyzer;
    }

    @RequestMapping(value="/quantity",method = RequestMethod.POST )
    public SentimentAnalyzeResult analyzeSentiment(@RequestBody String sentence){
        return quantitySentimentAnalyzer.analyze(sentence);
    }
}
