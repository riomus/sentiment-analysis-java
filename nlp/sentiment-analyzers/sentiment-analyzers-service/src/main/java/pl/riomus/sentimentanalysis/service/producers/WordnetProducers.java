package pl.riomus.sentimentanalysis.service.producers;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.riomus.wordnet.api.LexicalUnitForSynsetProvider;
import pl.riomus.wordnet.api.LexicalUnitSentimentProvider;
import pl.riomus.wordnet.client.WordnetServiceClient;
import pl.riomus.wordnet.sentiment.BasicLexicalUnitSentimentProvider;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
@Configuration
public class WordnetProducers {

    public static final String WORDNET_SERVICE_ADDRESS_PROPERTY = "wordnet.service.address";
    public static final String DEFAULT_WORDNET_ADDRESS = "http://wordnet:8080";

    @Bean
    public LexicalUnitForSynsetProvider createLexicalUnitForSynsetProvider(){
        return new WordnetServiceClient(System.getProperty(WORDNET_SERVICE_ADDRESS_PROPERTY, DEFAULT_WORDNET_ADDRESS));
    }

    @Bean
    public LexicalUnitSentimentProvider createLexicalUnitSentimentProvider(){
        return new BasicLexicalUnitSentimentProvider();
    }
}
