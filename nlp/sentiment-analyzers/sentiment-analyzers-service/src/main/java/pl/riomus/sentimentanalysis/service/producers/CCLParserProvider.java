package pl.riomus.sentimentanalysis.service.producers;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.riomus.nlp.api.CCLParser;
import pl.riomus.nlp.ccl.parser.BasicCCLParser;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
@Configuration
public class CCLParserProvider {
    @Bean
    public CCLParser createCCLParser(){
        return new BasicCCLParser();
    }
}
