package pl.riomus.sentimentanalysis.service.producers;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.riomus.nlp.api.CCLParser;
import pl.riomus.nlp.api.ContentTagger;
import pl.riomus.nlp.ccl.parser.BasicCCLParser;
import pl.riomus.nlp.wcrft2.Wcrft2ServiceClient;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
@Configuration
public class Wcrft2Producer {

    public static final String WCRFT2_SERVICE_ADDRESS_PROPERTY = "wcrft2.service.address";
    public static final String DEFAULT_SERVICE_ADDRESS = "http://wcrft2:8080";


    @Bean
    public Wcrft2ServiceClient createWcrft2ServiceClient(CCLParser cclParser){
        return new Wcrft2ServiceClient(System.getProperty(WCRFT2_SERVICE_ADDRESS_PROPERTY, DEFAULT_SERVICE_ADDRESS),cclParser);
    }
}
