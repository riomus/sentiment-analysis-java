package pl.riomus.sentimentanalysis.service.producers.analyzers;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.riomus.nlp.api.CCLContentDisambiguator;
import pl.riomus.nlp.wcrft2.Wcrft2ServiceClient;
import pl.riomus.sentimentanalysis.quantity.QuantitySentimentAnalyzer;
import pl.riomus.wordnet.api.LexicalUnitForSynsetProvider;
import pl.riomus.wordnet.api.LexicalUnitSentimentProvider;

import javax.inject.Inject;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
@Configuration
public class QuantityAnalyzerProducer {
    @Bean
    @Inject
    public QuantitySentimentAnalyzer createQuantitySentimentAnalyzer(LexicalUnitForSynsetProvider lexicalUnitForSynsetProvider,
                                                                     Wcrft2ServiceClient contentTagger,
                                                                     CCLContentDisambiguator cclContentDisambiguator,
                                                                     LexicalUnitSentimentProvider lexicalUnitSentimentProvider){
        return new QuantitySentimentAnalyzer(lexicalUnitForSynsetProvider,contentTagger,cclContentDisambiguator,lexicalUnitSentimentProvider);
    }
}
