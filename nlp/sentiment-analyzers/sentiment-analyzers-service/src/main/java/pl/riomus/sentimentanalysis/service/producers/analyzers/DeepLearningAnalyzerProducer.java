package pl.riomus.sentimentanalysis.service.producers.analyzers;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.riomus.nlp.sentimentanalysis.deeplearning.serialized.DeepLearningSentimentAnalyzer;
import pl.riomus.wordcoexistency.vectorizer.content.ComplexContentVectorizer;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
@Configuration
public class DeepLearningAnalyzerProducer {
    @Bean
    public DeepLearningSentimentAnalyzer createDeeplLearningAnalyzer(ComplexContentVectorizer contentVectorizer){
        return new DeepLearningSentimentAnalyzer(contentVectorizer);
    }
}
