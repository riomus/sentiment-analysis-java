package pl.riomus.sentimentanalysis.service.producers;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.riomus.nlp.api.wordcoexistence.CoexistencyEntryProvider;
import pl.riomus.nlp.api.wordcoexistence.WordVectorizer;
import pl.riomus.wordcoexistence.common.ToWordVectorTransformator;
import pl.riomus.wordcoexistence.redis.vectorizer.CoexistencyEntryBasedVectorizer;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
@Configuration
public class WordVectorizerProducer {

    @Bean
    public WordVectorizer createRedisWectorizer(CoexistencyEntryProvider coexistencyEntryProvider) {
        return new CoexistencyEntryBasedVectorizer(coexistencyEntryProvider,new ToWordVectorTransformator(coexistencyEntryProvider.getCoexsistenctVectorSize()));
    }

}
