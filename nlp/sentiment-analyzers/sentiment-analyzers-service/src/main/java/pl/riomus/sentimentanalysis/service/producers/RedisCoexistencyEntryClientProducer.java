package pl.riomus.sentimentanalysis.service.producers;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.riomus.nlp.api.wordcoexistence.CoexistencyEntryProvider;
import pl.riomus.wordcoexistence.redis.vectorizer.RedisCoexistencyEntryClient;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
@Configuration
public class RedisCoexistencyEntryClientProducer {

    public static final String REDIS_SERVICE_ADDRESS_PROPERTY = "redis.service.address";
    public static final String REDIS_SERVICE_PORT_PROPERTY = "redis.service.port";
    public static final String DEFAULT_SERVICE_ADDRESS = "redis";
    public static final String DEFAULT_SERVICE_PORT = "6379";


    @Bean
    public CoexistencyEntryProvider createRedisWectorizer() {
        return new RedisCoexistencyEntryClient(getServiceAddress(), getServicePort());
    }

    public String getServiceAddress() {
        return getProperty(REDIS_SERVICE_ADDRESS_PROPERTY, DEFAULT_SERVICE_ADDRESS);
    }

    private String getProperty(String propertyName, String defaultValue) {
        return System.getProperty(propertyName, defaultValue);
    }

    public int getServicePort() {
        return Integer.parseInt(getProperty(REDIS_SERVICE_PORT_PROPERTY, DEFAULT_SERVICE_PORT));
    }
}
