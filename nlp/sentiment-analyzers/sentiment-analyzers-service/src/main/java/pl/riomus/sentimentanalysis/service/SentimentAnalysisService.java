package pl.riomus.sentimentanalysis.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
@ComponentScan
@EnableAutoConfiguration
public class SentimentAnalysisService {
    public static void main(String[] args) throws Exception {
        SpringApplication.run(SentimentAnalysisService.class, args);
    }
}


