package pl.riomus.sentimentanalysis.service.producers.analyzers;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.riomus.nlp.api.ContentTagger;
import pl.riomus.nlp.sentimentanalysis.classifier.serialized.ClassifierSentimentAnalyzer;

import javax.inject.Inject;

/**
 * Created by Roman Bartusiak (roman.bartusiak@nsn.com)
 */
@Configuration
public class ClassifierAnalyzerProducer {

    @Bean
    @Inject
    public ClassifierSentimentAnalyzer createClassifierSentimentAnalyzer(ContentTagger tagger){
        return new ClassifierSentimentAnalyzer(tagger,1,2);
    }
}
