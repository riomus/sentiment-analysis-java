package pl.riomus.sentimentanalysis.service.producers;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.riomus.nlp.api.CCLContentDisambiguator;
import pl.riomus.nlp.api.CCLParser;
import pl.riomus.nlp.wsd.client.WsdServiceClient;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
@Configuration
public class WSDProducer {

    public static final String WSD_SERVICE_ADDRESS_PROPERTY = "wsd.service.address";
    public static final String DEFAULT_SERVICE_ADDRESS = "http://wsd:5000";


    @Bean
    public CCLContentDisambiguator createWsdServiceClient(CCLParser cclParser){
        return new WsdServiceClient(System.getProperty(WSD_SERVICE_ADDRESS_PROPERTY, DEFAULT_SERVICE_ADDRESS),cclParser);
    }
}
