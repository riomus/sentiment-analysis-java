package pl.riomus.sentimentanalysis.quantity;

import pl.riomus.nlp.api.CCLContentDisambiguator;
import pl.riomus.nlp.api.ContentTagger;
import pl.riomus.nlp.api.model.DisambiguatedContent;
import pl.riomus.nlp.api.model.SentimentAnalyzeResult;
import pl.riomus.nlp.api.model.TaggedContent;
import pl.riomus.nlp.api.model.ccl.Chunk;
import pl.riomus.nlp.api.model.ccl.Property;
import pl.riomus.nlp.api.model.ccl.Sentence;
import pl.riomus.nlp.api.model.ccl.Token;
import pl.riomus.nlp.api.sentimentanalyzers.SentimentAnalyzer;
import pl.riomus.wordnet.api.LexicalUnitForSynsetProvider;
import pl.riomus.wordnet.api.LexicalUnitSentimentProvider;
import pl.riomus.wordnet.api.models.LexicalUnit;
import pl.riomus.wordnet.api.models.Sentiment;

import java.util.*;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
public class QuantitySentimentAnalyzer implements SentimentAnalyzer{
    private static final Logger LOG = Logger.getLogger(QuantitySentimentAnalyzer.class.getCanonicalName());
    private LexicalUnitForSynsetProvider lexicalUnitForSynsetProvider;
    private ContentTagger contentTagger;
    private CCLContentDisambiguator cclContentDisambiguator;
    private LexicalUnitSentimentProvider lexicalUnitSentimentProvider;

    public QuantitySentimentAnalyzer(LexicalUnitForSynsetProvider lexicalUnitForSynsetProvider, ContentTagger contentTagger, CCLContentDisambiguator cclContentDisambiguator, LexicalUnitSentimentProvider lexicalUnitSentimentProvider) {
        this.lexicalUnitForSynsetProvider = lexicalUnitForSynsetProvider;
        this.contentTagger = contentTagger;
        this.cclContentDisambiguator = cclContentDisambiguator;
        this.lexicalUnitSentimentProvider = lexicalUnitSentimentProvider;
    }

    public SentimentAnalyzeResult analyze(String content) {
        LOG.info("Analizing sentence " + content);
        TaggedContent taggedContent = contentTagger.tag(content);
        DisambiguatedContent disambiguatedContent = cclContentDisambiguator.disambiguate(taggedContent);
        Map<String, Optional<Long>> synsetIdentifiers = getSynsetIdentifiers(disambiguatedContent);
        Map<String, Sentiment> lemmaSentiments = new HashMap<>();
        for (String lemma : synsetIdentifiers.keySet()) {
            Optional<Long> synsetId = synsetIdentifiers.get(lemma);
            LOG.info("Analizing lemma " + lemma + " with synset id " + synsetId);
            Optional<LexicalUnit> lexicalUnit = getLexicalUnitsForSynsetAndLemma(lemma, synsetId);
            if(lexicalUnit.isPresent()) {
                Sentiment lemmaSentiment = lexicalUnitSentimentProvider.getSentiment(lexicalUnit.get());
                lemmaSentiments.put(lemma, lemmaSentiment);
            }
        }
        return createAnalysisResult(lemmaSentiments);
    }

    private SentimentAnalyzeResult createAnalysisResult(Map<String, Sentiment> lemmaSentiments) {
        Sentiment outputSentiment = calculateOutputSentiment(lemmaSentiments);
        SentimentAnalyzeResult sentimentAnalyzeResult = new SentimentAnalyzeResult();
        sentimentAnalyzeResult.setSentiment(outputSentiment);
        sentimentAnalyzeResult.addMetadata("lemmaSentiments",lemmaSentiments);
        return sentimentAnalyzeResult;
    }

    private Sentiment calculateOutputSentiment(Map<String, Sentiment> lemmaSentiments) {
        Stream<Sentiment> sentimentsWithoutUnkonown = lemmaSentiments.values().parallelStream().filter(x -> !x.equals(Sentiment.UNKNOWN));
        Map<Sentiment, List<Sentiment>> sentimentGroups = sentimentsWithoutUnkonown.collect(Collectors.groupingBy(x -> x));
        Optional<List<Sentiment>> first = sentimentGroups.values().parallelStream().sorted((a, b) -> a.size() - b.size()).findFirst();
        return first.orElse(Arrays.asList(Sentiment.UNKNOWN)).get(0);
    }

    private Optional<LexicalUnit> getLexicalUnitsForSynsetAndLemma(String lemma, Optional<Long> synsetId) {
        if(synsetId.isPresent()) {
            return lexicalUnitForSynsetProvider.getLexicalUnitsForSynsetAndLemma(synsetId.get(), lemma);
        }
        else{
            return lexicalUnitForSynsetProvider.getLexicalUnit( lemma);
        }
    }


    public Map<String, Optional<Long>> getSynsetIdentifiers(DisambiguatedContent cclContent) {
        Map<String, Optional<Long>> synsetIdentifiers = new HashMap<>();
        for (Chunk chunk : cclContent.getContent().getChunks()) {
            for (Sentence sentence : chunk.getSentences()) {
                for (Token tok : sentence.getTokens()) {
                    if (tok.getProperties() != null) {
                        String lemma = tok.getLexicalUnit().getBase();
                        boolean added=false;
                        for (Property property : tok.getProperties()) {
                            if (property.getKey().equals("sense:ukb:syns_id")) {
                                added=true;
                                synsetIdentifiers.put(lemma, Optional.of(Long.parseLong(property.getValue())));
                            }
                        }
                        if(!added){
                            synsetIdentifiers.put(lemma,Optional.empty());
                        }
                    }
                }

            }

        }
        return synsetIdentifiers;
    }
}
