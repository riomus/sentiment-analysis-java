package pl.riomus.nlp.sentimentanalysis.classifier;

import pl.riomus.nlp.api.ContentTagger;
import pl.riomus.nlp.api.model.TaggedContent;
import pl.riomus.nlp.api.model.ccl.Chunk;
import pl.riomus.nlp.api.model.ccl.Sentence;
import pl.riomus.nlp.api.model.ccl.Token;
import pl.riomus.nlp.ccl.parser.BasicCCLParser;
import pl.riomus.nlp.wcrft2.Wcrft2ServiceClient;
import weka.core.tokenizers.Tokenizer;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Logger;

/**
 * Created by Roman Bartusiak (roman.bartusiak@nsn.com)
 */
public class WCRFTTokenizer extends Tokenizer{
    private static final long serialVersionUID=961275000570895606l;
    private static final Logger LOG=Logger.getLogger(WCRFTTokenizer.class.getCanonicalName());
    private static final String GLOBAL_INFO="WCRFT Based Tokenizer";
    private static final String REVISION="WCRFT 1.0 SNAPSHOT";
    private transient ContentTagger contentTagger;
    private String wcrft2ServiceAddress;
    private transient Iterator<String> tokensIterator;

    public WCRFTTokenizer(ContentTagger contentTagger,String wcrft2ServiceAddress) {
        this.contentTagger = contentTagger;
        this.wcrft2ServiceAddress = wcrft2ServiceAddress;
    }

    @Override
    public String globalInfo() {
        return GLOBAL_INFO;
    }

    @Override
    public boolean hasMoreElements() {
        return tokensIterator.hasNext();
    }

    @Override
    public String nextElement() {
        return tokensIterator.next();
    }

    @Override
    public void tokenize(String s) {
        ArrayList<String>  tokens = new ArrayList<String>();
        TaggedContent taggedContent =  contentTagger.tag(s);
        if(taggedContent!=null&&taggedContent.getContent()!=null&&taggedContent.getContent().getChunks()!=null)
        for (Chunk chunk : taggedContent.getContent().getChunks()) {
            for (Sentence sentence : chunk.getSentences()) {
                for (Token token : sentence.getTokens()) {
                    String base = token.getLexicalUnit().getBase().trim();
                    if(!base.isEmpty()) {
                        tokens.add(base);
                    }
                }

            }

        }
        tokensIterator = tokens.iterator();
    }

    @Override
    public String getRevision() {
        return REVISION;
    }


    private void readObject(ObjectInputStream inputStream)
            throws IOException, ClassNotFoundException
    {
        inputStream.defaultReadObject();
       contentTagger =new Wcrft2ServiceClient(wcrft2ServiceAddress,new BasicCCLParser());
    }


}
