package pl.riomus.nlp.sentimentanalysis.classifier;

import weka.core.tokenizers.Tokenizer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by Roman Bartusiak (roman.bartusiak@nsn.com)
 */
public class NGramTokenizer extends Tokenizer {
    private static final Logger LOG=Logger.getLogger(NGramTokenizer.class.getCanonicalName());
    private static final String BASE_INFO="NGram tokenizer";
    private static final String REVISION="1.0 Snapshot";
    public static final String LETTERS_AND_NUMBERS_REGEXP = "[A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ0-9]+";
    private Tokenizer baseTokenizer;
    private Integer minGramSize;
    private Integer maxGramSize;
    private transient Iterator<String> tokensIterator;
    public NGramTokenizer(Tokenizer baseTokenizer,Integer minGramSize,Integer maxGramSize) {
        this.baseTokenizer = baseTokenizer;
        this.minGramSize = minGramSize;
        this.maxGramSize = maxGramSize;
    }

    @Override
    public String globalInfo() {
        return BASE_INFO;
    }

    @Override
    public boolean hasMoreElements() {
        return tokensIterator.hasNext();
    }

    @Override
    public String nextElement() {
        return tokensIterator.next();
    }

    @Override
    public void tokenize(String s) {
        baseTokenizer.tokenize(s);
        List tokens=new ArrayList<>();
       List baseTokens=new ArrayList<>();
        while(baseTokenizer.hasMoreElements()){
            String token =(String) baseTokenizer.nextElement();
            if(!token.isEmpty()&&token.matches(LETTERS_AND_NUMBERS_REGEXP))
            baseTokens.add(token);
        }
        for(int gramModifier=0;gramModifier<=maxGramSize-minGramSize;gramModifier++){
            for(int i =0;i<=baseTokens.size()-maxGramSize+gramModifier;i++){
                String res="";
                for(int x=0;x<maxGramSize-gramModifier;x++){
                    res+=" ";
                    res+=baseTokens.get(i+x);
                }
                tokens.add(res.trim());
            }
        }
        tokensIterator=tokens.iterator();
    }

    @Override
    public String getRevision() {
        return REVISION;
    }
}
