package pl.riomus.nlp.sentimentanalysis.classifier.serialized;

import org.junit.Ignore;
import org.junit.Test;
import pl.riomus.allegro.data.client.AllegroDataServiceClient;
import pl.riomus.allegro.data.comment.AllegroComment;
import pl.riomus.allegro.data.comment.AllegroCommentType;
import pl.riomus.nlp.ccl.parser.BasicCCLParser;
import pl.riomus.nlp.wcrft2.Wcrft2ServiceClient;
import pl.riomus.sentimentanalyzers.common.validation.SentimentAnalyzerValidator;

import java.util.Collection;

public class ClassifierSentimentAnalyzerTest {

    @Test
    @Ignore
    public void classifierTest() throws Exception {
        ClassifierSentimentAnalyzer classifierSentimentAnalyzer = new ClassifierSentimentAnalyzer(new Wcrft2ServiceClient("http://localhost:8080", new BasicCCLParser()), 1, 2);
        AllegroDataServiceClient allegroDataServiceClient = new AllegroDataServiceClient("http://localhost:8090");

        Collection<AllegroComment> negComments = allegroDataServiceClient.getCommentsFromRange(AllegroCommentType.NEG, 110723, 1000).getContent();
        System.out.println("Negative size:" + negComments.size());
        Collection<AllegroComment> posComments = allegroDataServiceClient.getCommentsFromRange(AllegroCommentType.POS, 110723, 1000).getContent();
        System.out.println("Positive size:" + posComments.size());
        new SentimentAnalyzerValidator().validateClassifier(classifierSentimentAnalyzer, negComments, posComments);

    }
}