package pl.riomus.nlp.sentimentanalysis.classifier.serialized;


import com.google.common.collect.ImmutableMap;
import pl.riomus.nlp.api.CCLParser;
import pl.riomus.nlp.api.ContentTagger;
import pl.riomus.nlp.api.model.SentimentAnalyzeResult;
import pl.riomus.nlp.api.sentimentanalyzers.SentimentAnalyzer;
import pl.riomus.nlp.sentimentanalysis.classifier.NGramTokenizer;
import pl.riomus.nlp.sentimentanalysis.classifier.WCRFTTokenizer;
import pl.riomus.wordnet.api.models.Sentiment;
import weka.classifiers.meta.FilteredClassifier;
import weka.core.*;
import weka.core.tokenizers.Tokenizer;
import weka.filters.unsupervised.attribute.StringToWordVector;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
public class ClassifierSentimentAnalyzer implements SentimentAnalyzer{
    private static final Map<String,Sentiment> SENTIMENTS= ImmutableMap.of("POS",Sentiment.POSITIVE,"NEG",Sentiment.NEGATIVE);
    private final FilteredClassifier classifier;
    private final Instance serializedInstance;
    public ClassifierSentimentAnalyzer(){
        classifier = loadClassifier();
        serializedInstance=loadInstance();
    }

    private Instance loadInstance() {
        try {
            return (Instance) SerializationHelper.read(ClassifierSentimentAnalyzer.class.getClassLoader().getResourceAsStream("instance"));
        }catch(Exception e){
            throw new FailedToDeserializeClassifierException(e);
        }
    }

    public ClassifierSentimentAnalyzer(Tokenizer tokenizer){
        this();
        StringToWordVector filter = (StringToWordVector) classifier.getFilter();
        filter.setTokenizer(tokenizer);
    }

    public ClassifierSentimentAnalyzer(ContentTagger tagger,int nGramMinSize,int nGramMaxSize){
        this(new NGramTokenizer(new WCRFTTokenizer(tagger,""),nGramMinSize,nGramMaxSize));
    }

    private FilteredClassifier loadClassifier() {
        try {
            return (FilteredClassifier) SerializationHelper.read(ClassifierSentimentAnalyzer.class.getClassLoader().getResourceAsStream("smo_200k"));
        }catch(Exception e){
            throw new FailedToDeserializeClassifierException(e);
        }
    }

    public SentimentAnalyzeResult analyze(String content){
        try {
            return tryToAnalyze(content);
        }catch(Exception e){
            throw new ClassifierException(e);
        }
    }

    private SentimentAnalyzeResult tryToAnalyze(String content) throws Exception {
        Instance instance=(Instance)serializedInstance.copy();
        instance.setValue(instance.attribute(4),content);
        SentimentAnalyzeResult sentimentAnalyzeResult = new SentimentAnalyzeResult();
        Double classification = classifier.classifyInstance(instance);
        String classifiedClass = serializedInstance.attribute(3).value(classification.intValue());
        sentimentAnalyzeResult.setSentiment(SENTIMENTS.get(classifiedClass));
        return sentimentAnalyzeResult;
    }

}
