package pl.riomus.nlp.sentimentanalysis.classifier.serialized;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
public class FailedToDeserializeClassifierException extends RuntimeException {
    public FailedToDeserializeClassifierException(Exception e) {
        super(e);
    }
}
