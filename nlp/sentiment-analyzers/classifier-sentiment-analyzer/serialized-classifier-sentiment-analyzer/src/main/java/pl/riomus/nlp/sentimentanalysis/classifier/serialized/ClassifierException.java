package pl.riomus.nlp.sentimentanalysis.classifier.serialized;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
public class ClassifierException extends RuntimeException {
    public ClassifierException(Exception e) {
        super(e);
    }
}
