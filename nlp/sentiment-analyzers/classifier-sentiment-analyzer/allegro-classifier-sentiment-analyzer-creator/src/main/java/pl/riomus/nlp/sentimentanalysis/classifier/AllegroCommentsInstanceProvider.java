package pl.riomus.nlp.sentimentanalysis.classifier;

import weka.core.Instances;
import weka.experiment.InstanceQuery;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.NominalToString;

import java.util.Optional;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * Created by Roman Bartusiak (roman.bartusiak@nsn.com)
 */
public class AllegroCommentsInstanceProvider {
    private static final Logger LOG=Logger.getLogger(AllegroCommentsInstanceProvider.class.getCanonicalName());

    private Properties properties;

    public AllegroCommentsInstanceProvider() {
    }

    public Instances getComments(Optional<String> query) {
        try {
            properties = new Properties();
            properties.load(AllegroCommentsInstanceProvider.class.getClassLoader().getResourceAsStream("db.properties"));
            return tryToGetComments(query);
        } catch (Exception e) {
            throw new AllegroCommentFetchingException(e);
        }
    }

    private Instances tryToGetComments(Optional<String> query) throws Exception {
        InstanceQuery instanceQuery = new InstanceQuery();
        instanceQuery.setUsername(getProperty("db.user"));
        instanceQuery.setPassword(getProperty("db.password"));
        instanceQuery.setQuery(query.orElse(getProperty("db.query")));
        instanceQuery.setDatabaseURL("jdbc:mysql://156.17.131.243:3307/allegro?useUnicode=true&characterEncoding=utf-8");
        Instances instances = instanceQuery.retrieveInstances();
        instances.setClassIndex(3);
        NominalToString nominalToString=new NominalToString();
        nominalToString.setAttributeIndexes("last");
        nominalToString.setInputFormat(instances);
        instances=Filter.useFilter(instances,nominalToString);
        LOG.info(String.format("Received %d allegro comments",instances.numInstances()));
        return instances;
    }

    public String getProperty(String key) {
        return System.getProperty(key, properties.getProperty(key));
    }
}
