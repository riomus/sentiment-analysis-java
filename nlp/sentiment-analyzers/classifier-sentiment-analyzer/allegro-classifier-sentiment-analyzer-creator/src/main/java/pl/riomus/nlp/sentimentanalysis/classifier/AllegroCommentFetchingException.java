package pl.riomus.nlp.sentimentanalysis.classifier;

/**
 * Created by Roman Bartusiak (roman.bartusiak@nsn.com)
 */
public class AllegroCommentFetchingException extends RuntimeException {
    public AllegroCommentFetchingException(Throwable throwable) {
        super(throwable);
    }
}
