package pl.riomus.nlp.sentimentanalysis.classifier;

import pl.riomus.nlp.ccl.parser.BasicCCLParser;
import pl.riomus.nlp.wcrft2.Wcrft2ServiceClient;
import weka.classifiers.Classifier;
import weka.classifiers.meta.FilteredClassifier;
import weka.filters.unsupervised.attribute.StringToWordVector;

/**
 * Created by Roman Bartusiak (roman.bartusiak@nsn.com)
 */
public class NGramClassifierProvider {


    public FilteredClassifier createEmptyClassifier(Integer minGramSize,Integer maxGramSize,String textAttributeIndices,Classifier classifier){
        StringToWordVector stringToWordVector=new StringToWordVector();
        stringToWordVector.setAttributeIndices(textAttributeIndices);
        NGramTokenizer nGramTokenizer = new NGramTokenizer(new WCRFTTokenizer(new Wcrft2ServiceClient(getWcrftServiceAddress(), new BasicCCLParser()),getWcrftServiceAddress()), minGramSize,maxGramSize);
        stringToWordVector.setTokenizer(nGramTokenizer);
        stringToWordVector.setWordsToKeep(10000000);
        FilteredClassifier filteredClassifier = new FilteredClassifier();
        filteredClassifier.setFilter(stringToWordVector);
        filteredClassifier.setClassifier(classifier);
        return filteredClassifier;
    }

    public String getWcrftServiceAddress() {
        return System.getProperty("wcrft2.address","http://localhost:8080");
    }
}
