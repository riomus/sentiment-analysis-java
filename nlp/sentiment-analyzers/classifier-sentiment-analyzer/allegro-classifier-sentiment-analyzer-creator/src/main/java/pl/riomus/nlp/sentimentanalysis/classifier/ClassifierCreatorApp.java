package pl.riomus.nlp.sentimentanalysis.classifier;

import weka.classifiers.Classifier;
import weka.classifiers.functions.SMO;
import weka.classifiers.functions.supportVector.PolyKernel;
import weka.classifiers.meta.FilteredClassifier;
import weka.core.Debug;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.SerializationHelper;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.RenameAttribute;

import java.io.FileReader;
import java.util.Optional;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
public class ClassifierCreatorApp {

    public static final String OUTPUT_CLASSIFIER = "/home/riomus/github/sentiment-analysis/java/nlp/sentiment-analyzers/classifier-sentiment-analyzer/serialized-classifier-sentiment-analyzer/src/main/resources/smo_200k_k_2";
    public static final String OUTPUT_INSTANCE = "/home/riomus/github/sentiment-analysis/java/nlp/sentiment-analyzers/classifier-sentiment-analyzer/serialized-classifier-sentiment-analyzer/src/main/resources/instance";

    public static void testRead() throws Exception{
        Object read = SerializationHelper.read(OUTPUT_CLASSIFIER);
        FilteredClassifier classifier=(FilteredClassifier)read;
        Instance i=(Instance)SerializationHelper.read(OUTPUT_INSTANCE);
        i.setValue(i.attribute(4),"Jestem niezwykle zły, nic mi się nie chce");
        System.out.println(i.attribute(4));
        Double classification = classifier.classifyInstance(i);
        System.out.println(i.attribute(3).value(classification.intValue()));
    }

    public static void main(String[] args) throws Exception{
        createClassifier();

    }

    private static void createClassifier() throws Exception {
        System.out.println(Debug.getCurrentDir());
        System.out.println(Debug.getHomeDir());
        AllegroCommentsInstanceProvider allegroCommentsInstanceProvider=new AllegroCommentsInstanceProvider();
        Instances comments = allegroCommentsInstanceProvider.getComments(Optional.empty());
        RenameAttribute rename=new RenameAttribute();
        rename.setAttributeIndices("first-last");
        rename.setFind("id");
        rename.setReplace("id-att");
        rename.setInputFormat(comments);
        comments= Filter.useFilter(comments, rename);
        rename.setFind("type");
        rename.setReplace("type-att");
        rename.setInputFormat(comments);
        comments=Filter.useFilter(comments,rename);
        rename.setFind("content");
        rename.setReplace("content-att");
        rename.setInputFormat(comments);
        comments=Filter.useFilter(comments,rename);
        rename.setFind("authorId");
        rename.setReplace("authorId-att");
        rename.setInputFormat(comments);
        comments=Filter.useFilter(comments,rename);
        rename.setFind("reciverId");
        rename.setReplace("reciverId-att");
        rename.setInputFormat(comments);
        comments=Filter.useFilter(comments,rename);
        NGramClassifierProvider nGramClassifierProvider=new NGramClassifierProvider();
        SMO classifier = new SMO();
        PolyKernel kernel = new PolyKernel();
        kernel.setExponent(2);
        classifier.setKernel(kernel);
        FilteredClassifier nGramClassifier = nGramClassifierProvider.createEmptyClassifier(1,2, "last", classifier);
//        Evaluation evaluation=new Evaluation(comments);
//        evaluation.crossValidateModel(nGramClassifier, comments, 4, new Debug.Random(1));
//        System.out.println(eval-XX:-UseGCOverheadLimituation.toSummaryString());
//        System.out.println(evaluation.toClassDet2ailsString());
        nGramClassifier.buildClassifier(comments);
        Instance i=comments.firstInstance();
        i.setValue(i.attribute(4),"Ten alegrowicz to dno, nie zapłacił i jeszcze przezywa");
        System.out.println(i);
        Double classified = nGramClassifier.classifyInstance(i);
        System.out.println(i.attribute(3).value(classified.intValue()));
        Debug.saveToFile(OUTPUT_CLASSIFIER, nGramClassifier);
        Debug.saveToFile(OUTPUT_INSTANCE, i);
    }
}
