package pl.riomus.sentimentanalyzers.common.validation;

import com.google.common.collect.ImmutableMap;
import pl.riomus.allegro.data.comment.AllegroComment;
import pl.riomus.allegro.data.comment.AllegroCommentType;
import pl.riomus.nlp.api.model.SentimentAnalyzeResult;
import pl.riomus.nlp.api.sentimentanalyzers.SentimentAnalyzer;
import pl.riomus.wordnet.api.models.Sentiment;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
public class SentimentAnalyzerValidator {

    private static final Logger LOG=Logger.getLogger(SentimentAnalyzerValidator.class.getCanonicalName());

    private static final Map<Sentiment,AllegroCommentType> SENTIMENT_TO_ALLEGRO_COMMENT_TYPE= ImmutableMap.of(Sentiment.NEGATIVE, AllegroCommentType.NEG, Sentiment.POSITIVE, AllegroCommentType.POS, Sentiment.UNKNOWN, AllegroCommentType.UNKNOWN,Sentiment.NEUTRAL,AllegroCommentType.NEU);

    public  SentimentAnalyzerValidationResult validateClassifier(SentimentAnalyzer classifierSentimentAnalyzer, Collection<AllegroComment> negComments, Collection<AllegroComment> posComments) {
        double[] negativeRow = {0.,0.};
        double[] positiveRow = {0., 0.};

        testClassigierForDataPart(classifierSentimentAnalyzer, negComments, negativeRow);
        testClassigierForDataPart(classifierSentimentAnalyzer, posComments, positiveRow);

       LOG.info("Negative classification: " + Arrays.toString(negativeRow));
        LOG.info("Positive classification: " + Arrays.toString(positiveRow));

        double recall=(positiveRow[0]+negativeRow[0])/(positiveRow[0]+positiveRow[1]+negativeRow[1]+negativeRow[0]);
        double recallPos = positiveRow[0] / Math.max(1.,(positiveRow[0] + negativeRow[1]));
        double recallNeg = negativeRow[0] / Math.max(1.,(negativeRow[0] + positiveRow[1]));
        double precision=(recallPos + recallNeg)/2.;
        SentimentAnalyzerValidationResult sentimentAnalyzerValidationResult = new SentimentAnalyzerValidationResult(recall, precision);

        LOG.info(sentimentAnalyzerValidationResult.toString());

        return sentimentAnalyzerValidationResult;
    }

    private void testClassigierForDataPart(SentimentAnalyzer classifierSentimentAnalyzer, Collection<AllegroComment> comments, double[] resultRow) {
        for (AllegroComment comment : comments) {
            SentimentAnalyzeResult analyzeResult = classifierSentimentAnalyzer.analyze(comment.getContent());
            boolean classificationWasCorrect = SENTIMENT_TO_ALLEGRO_COMMENT_TYPE.get(analyzeResult.getSentiment()).equals(comment.getType());
           LOG.info("Classification was correct:" + classificationWasCorrect + " received:" + analyzeResult.getSentiment() + " is:" + comment.getType());

                if (classificationWasCorrect) {
                    resultRow[0]++;
                } else {
                    resultRow[1]++;
                }
        }
    }
}
