package pl.riomus.sentimentanalyzers.common.validation;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
public class SentimentAnalyzerValidationResult {
    private double recall, precision;

    public SentimentAnalyzerValidationResult(double recall,double precision) {
        this.recall = recall;
        this.precision = precision;
    }

    public double getRecall() {
        return recall;
    }

    public double getFscore() {
        return 2.*precision*recall/(precision+recall);
    }

    public double getPrecision() {
        return precision;
    }

    @Override
    public String toString() {
        return "SentimentAnalyzerValidationResult{" +
                "recall=" + recall +
                ", fscore=" + getFscore() +
                ", precision=" + precision +
                '}';
    }

}
