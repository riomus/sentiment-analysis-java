package pl.riomus.nlp.sentimentanalysis.deeplearning.serialized;

import org.junit.Ignore;
import org.junit.Test;
import pl.riomus.allegro.data.client.AllegroDataServiceClient;
import pl.riomus.allegro.data.comment.AllegroComment;
import pl.riomus.allegro.data.comment.AllegroCommentType;
import pl.riomus.nlp.ccl.parser.BasicCCLParser;
import pl.riomus.nlp.wcrft2.Wcrft2ServiceClient;
import pl.riomus.sentimentanalyzers.common.validation.SentimentAnalyzerValidator;
import pl.riomus.wordcoexistence.redis.vectorizer.RedisWordVectorizer;
import pl.riomus.wordcoexistency.vectorizer.content.ComplexContentVectorizer;

import java.util.Collection;


public class DeepLearningSentimentAnalyzerTest {

    @Test
    @Ignore
    public void classifierTest() throws Exception {
        DeepLearningSentimentAnalyzer classifierSentimentAnalyzer = new DeepLearningSentimentAnalyzer(new ComplexContentVectorizer(new Wcrft2ServiceClient("http://localhost:8080", new BasicCCLParser()), new RedisWordVectorizer("localhost", 6379)));
        AllegroDataServiceClient allegroDataServiceClient = new AllegroDataServiceClient("http://localhost:8090");

        Collection<AllegroComment> negComments = allegroDataServiceClient.getCommentsFromRange(AllegroCommentType.NEG, 0, 100).getContent();
        System.out.println("Negative size:" + negComments.size());
        Collection<AllegroComment> posComments = allegroDataServiceClient.getCommentsFromRange(AllegroCommentType.POS, 0, 100).getContent();
        System.out.println("Positive size:" + posComments.size());
        new SentimentAnalyzerValidator().validateClassifier(classifierSentimentAnalyzer, negComments, posComments);

    }
}