package pl.riomus.nlp.sentimentanalysis.deeplearning.serialized;

import org.deeplearning4j.models.classifiers.dbn.DBN;
import org.deeplearning4j.util.SerializationUtils;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.NDArrayFactory;
import pl.riomus.nlp.api.contentvectorizer.ContentVectorizer;
import pl.riomus.nlp.api.model.SentimentAnalyzeResult;
import pl.riomus.nlp.api.sentimentanalyzers.SentimentAnalyzer;
import pl.riomus.sentimentanalysis.deeplearning.common.SentimentLabelProvider;
import pl.riomus.sentimentanalysis.deeplearning.common.VectorUtils;

import java.util.Arrays;

/**
 * Created by Roman Bartusiak (roman.bartusiak@nsn.com)
 */
public class DeepLearningSentimentAnalyzer implements SentimentAnalyzer {

    DBN deepBeliveNeuralNetwork;
    private ContentVectorizer contentVectorizer;
    private SentimentLabelProvider sentimentLabelProvider;
    private VectorUtils vectorUtils;

    public DeepLearningSentimentAnalyzer(ContentVectorizer contentVectorizer) {
        this((DBN) SerializationUtils.readObject(DeepLearningSentimentAnalyzer.class.getClassLoader().getResourceAsStream("dbn.ser")), contentVectorizer, new SentimentLabelProvider(),new VectorUtils(250));

    }

    public DeepLearningSentimentAnalyzer(DBN deepBeliveNeuralNetwork, ContentVectorizer contentVectorizer, SentimentLabelProvider sentimentLabelProvider,VectorUtils vectorUtils) {
        this.deepBeliveNeuralNetwork = deepBeliveNeuralNetwork;
        this.contentVectorizer = contentVectorizer;
        this.sentimentLabelProvider = sentimentLabelProvider;
        this.vectorUtils = vectorUtils;
    }

    @Override
    public SentimentAnalyzeResult analyze(String content) {
        INDArray contentArray = vectorUtils.createFeatureArray(contentVectorizer.vectorize(content));
        int[] labels = deepBeliveNeuralNetwork.predict(contentArray);
        INDArray indArray = deepBeliveNeuralNetwork.labelProbabilities(contentArray);
        SentimentAnalyzeResult sentimentAnalyzeResult = new SentimentAnalyzeResult();
        System.out.println(indArray);
        sentimentAnalyzeResult.setSentiment(sentimentLabelProvider.getLabel(labels[0]));
        return sentimentAnalyzeResult;
    }


}

