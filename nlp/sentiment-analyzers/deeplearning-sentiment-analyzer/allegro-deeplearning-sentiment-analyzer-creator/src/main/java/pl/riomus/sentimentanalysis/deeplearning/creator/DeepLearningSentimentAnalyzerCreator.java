package pl.riomus.sentimentanalysis.deeplearning.creator;

import org.apache.commons.math3.random.MersenneTwister;
import org.apache.commons.math3.random.RandomGenerator;
import org.deeplearning4j.datasets.iterator.DataSetFetcher;
import org.deeplearning4j.datasets.iterator.DataSetIterator;
import org.deeplearning4j.datasets.iterator.impl.ListDataSetIterator;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.models.classifiers.dbn.DBN;
import org.deeplearning4j.nn.WeightInit;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.util.SerializationUtils;
import org.nd4j.linalg.api.activation.Activations;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.api.DataSet;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import pl.riomus.allegro.data.client.AllegroDataServiceClient;
import pl.riomus.nlp.ccl.parser.BasicCCLParser;
import pl.riomus.nlp.wcrft2.Wcrft2ServiceClient;
import pl.riomus.sentimentanalysis.deeplearning.common.SentimentLabelProvider;
import pl.riomus.sentimentanalysis.deeplearning.common.VectorUtils;
import pl.riomus.wordcoexistence.redis.vectorizer.RedisWordVectorizer;
import pl.riomus.wordcoexistency.vectorizer.content.ComplexContentVectorizer;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Arrays;
import java.util.logging.Logger;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
public class DeepLearningSentimentAnalyzerCreator {

    private static final Logger LOG=Logger.getLogger(DeepLearningSentimentAnalyzerCreator.class.getCanonicalName());
    public static final String FILE_OUT = "/home/riomus/github/sentiment-analysis/java/nlp/sentiment-analyzers/deeplearning-sentiment-analyzer/serialized-deeplearning-sentiment-analyzer/src/main/resources/dbn.ser_test";
    public static final int ALLEGRO_PAGE_SIZE = 5;

    public static void main(String[] args) throws Exception{
        RandomGenerator gen = new MersenneTwister(123);
        ComplexContentVectorizer complexContentVectorizer = new ComplexContentVectorizer(new Wcrft2ServiceClient("http://localhost:8080", new BasicCCLParser()), new RedisWordVectorizer("localhost", 6379));
        DataSetFetcher dataSetFetcher=new AllegroCommentsDataFetcher(complexContentVectorizer,new AllegroDataServiceClient("http://localhost:8090"), ALLEGRO_PAGE_SIZE);
        dataSetFetcher.fetch(1);
        DataSet d2 = dataSetFetcher.next();
        LOG.info("First data part loaded.");
        NeuralNetConfiguration conf =  new NeuralNetConfiguration.Builder()
                .momentum(5e-1f).constrainGradientToUnitNorm(false).iterations(100)
                .withActivationType(NeuralNetConfiguration.ActivationType.SAMPLE)
                .lossFunction(LossFunctions.LossFunction.SQUARED_LOSS).rng(gen)
                .learningRate(0.1f).nIn(d2.numInputs()).nOut(d2.numOutcomes()).build();
       LOG.info("Inputs: "+d2.numInputs()+" Output: "+d2.numOutcomes());

        DBN d = new DBN.Builder().configure(conf)
                .hiddenLayerSizes(new int[]{600,300, 20})
                .build();

        d.getOutputLayer().conf().setNumIterations(10);
        NeuralNetConfiguration.setClassifier(d.getOutputLayer().conf());
        LOG.info("DBN configured");
        int i=0;
        while(i++<10) {
            d.fit(d2);
            System.gc();
            LOG.info("DBN fitted to samples");
            dataSetFetcher.fetch(1);
            d2 = dataSetFetcher.next();
            LOG.info("Next samples loaded");
            System.gc();
        }

        INDArray predict2 = d.output(d2.getFeatureMatrix());

        Evaluation eval = new Evaluation();
        eval.eval(d2.getLabels(),predict2);
        LOG.info(eval.stats());
        int[] predict = d.predict(d2.getFeatureMatrix());
         LOG.info("Predict " + Arrays.toString(predict));

        VectorUtils vectorUtils=new VectorUtils(1714);
        String testSentence = "Jestem zły i nic mi się nie chce!";
        double[][] vectorize = complexContentVectorizer.vectorize(testSentence);
        INDArray featureArray = vectorUtils.createFeatureArray(vectorize);
        SentimentLabelProvider sentimentLabelProvider = new SentimentLabelProvider();
        int[] predict1 = d.predict(featureArray);
        LOG.info(testSentence + " " + sentimentLabelProvider.getLabel(predict1[0]));

        testSentence = "Jestem zadowlony niezmiernie!";
         vectorize = complexContentVectorizer.vectorize(testSentence);
        featureArray = vectorUtils.createFeatureArray(vectorize);
        predict1 = d.predict(featureArray);
        LOG.info(testSentence + " " + sentimentLabelProvider.getLabel(predict1[0]));



        SerializationUtils.saveObject(d,new File(FILE_OUT));
    }
}
