package pl.riomus.sentimentanalysis.deeplearning.creator;

import org.deeplearning4j.datasets.fetchers.BaseDataFetcher;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import pl.riomus.allegro.data.client.AllegroDataServiceClient;
import pl.riomus.allegro.data.comment.AllegroComment;
import pl.riomus.allegro.data.comment.AllegroCommentType;
import pl.riomus.nlp.api.contentvectorizer.ContentVectorizer;
import pl.riomus.sentimentanalysis.deeplearning.common.SentimentLabelProvider;
import pl.riomus.sentimentanalysis.deeplearning.common.VectorUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
public class AllegroCommentsDataFetcher extends BaseDataFetcher {
    private static final Logger LOG= Logger.getLogger(AllegroCommentsDataFetcher.class.getCanonicalName());
    private static final List<AllegroCommentType> ALLEGRO_COMMENT_TYPES = Arrays.asList(AllegroCommentType.values());
    public static final int TOTAL_COMMENTS = Integer.MAX_VALUE;
    private  int pageSize;
    private ContentVectorizer contentVectorizer;
    private SentimentLabelProvider sentimentLabelProvider;
    private AllegroDataServiceClient allegroDataServiceClient;
       private VectorUtils vectorUtils=new VectorUtils(1714);
    public AllegroCommentsDataFetcher(ContentVectorizer contentVectorizer, AllegroDataServiceClient allegroDataServiceClient,int pageSize) {
        this.contentVectorizer = contentVectorizer;
        this.allegroDataServiceClient = allegroDataServiceClient;
        totalExamples = TOTAL_COMMENTS;
        cursor = 0;
        this.pageSize=pageSize;
        numOutcomes=2;
    }

    @Override
    public void fetch(int pages) {
        List<AllegroComment> comments = new ArrayList<>();
        for(int i=0;i<pages;i++) {
            comments.addAll(allegroDataServiceClient.getComments(AllegroCommentType.NEG, cursor , pageSize).getContent());
            comments.addAll(allegroDataServiceClient.getComments(AllegroCommentType.POS, cursor , pageSize).getContent());
            cursor +=1;
        }
        List<DataSet> toConvert = comments.stream().map(this::createDataSet).collect(Collectors.toList());
        initializeCurrFromList(toConvert);
    }

    private DataSet createDataSet(AllegroComment comment) {
       LOG.info("Fetched comment: " + comment.getContent() + " Type:" + comment.getType());
        double[][] vectorizedContent = contentVectorizer.vectorize(comment.getContent());
        INDArray featureVector = vectorUtils.createFeatureArray(vectorizedContent);
        inputColumns=featureVector.columns();
        INDArray labelVector = createOutputVector(ALLEGRO_COMMENT_TYPES.indexOf(comment.getType()));
        return new DataSet(featureVector, labelVector);
    }

    public INDArray createOutputVector(int i){
        return super.createOutputVector(i);
    }

}
