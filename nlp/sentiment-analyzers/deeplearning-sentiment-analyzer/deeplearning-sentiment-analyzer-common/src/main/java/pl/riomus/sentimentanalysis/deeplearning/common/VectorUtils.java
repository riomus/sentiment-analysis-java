package pl.riomus.sentimentanalysis.deeplearning.common;

import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.NDArrayFactory;
import org.nd4j.linalg.factory.Nd4j;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
public class VectorUtils {

    private int shrinkFactor = 300;

    public VectorUtils(int shrinkFactor) {
        this.shrinkFactor = shrinkFactor;
    }

    public VectorUtils() {
    }

    public double[] shrinkVector(double[] data) {
        int newSize=(int)Math.ceil(data.length/ shrinkFactor);
        double[] out=new double[newSize];
        for(int i=0;i<newSize;i++){
            int index=i*shrinkFactor;
            out[i]=0.;
            for(int x=0;x<shrinkFactor;x++){
                out[i]+=data[x+index];
            }
        }
        return out;
    }

    public INDArray createFeatureArray(double[][] vectorizedContent){
        INDArray featureVector = Nd4j.create(shrinkVector(vectorizedContent[0]), NDArrayFactory.C);

        for(int i=0;i<vectorizedContent.length;i++){
            featureVector.add(Nd4j.create(shrinkVector(vectorizedContent[i]),NDArrayFactory.C));
        }
        return featureVector;
    }
}
