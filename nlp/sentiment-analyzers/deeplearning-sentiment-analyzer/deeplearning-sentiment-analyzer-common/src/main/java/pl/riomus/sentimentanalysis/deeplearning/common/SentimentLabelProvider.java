package pl.riomus.sentimentanalysis.deeplearning.common;

import pl.riomus.wordnet.api.models.Sentiment;

import java.util.Arrays;
import java.util.List;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
public class SentimentLabelProvider {
    private static final List<Sentiment> SENTIMENTS= Arrays.asList(Sentiment.values());

    public int getLabel(Sentiment sentiment){
        return SENTIMENTS.indexOf(sentiment);
    }

    public Sentiment getLabel(int label){
        return SENTIMENTS.get(label);
    }
}
