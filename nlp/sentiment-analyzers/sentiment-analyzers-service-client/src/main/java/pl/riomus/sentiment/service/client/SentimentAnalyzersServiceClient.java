package pl.riomus.sentiment.service.client;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import pl.riomus.nlp.api.model.SentimentAnalyzeResult;

import java.nio.charset.Charset;
import java.util.Optional;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
public class SentimentAnalyzersServiceClient implements SentimentAnalyzers{
    public static final MediaType MEDIA_TYPE_JSON_UTF_8 = new MediaType("application", "json", Charset.forName("UTF-8"));
    public static final String CLASSIFIER_ADDRESS = "/classifier";
    private static final String QUANTITY_ADDRESS="/quantity";
    private static final String DEEP_ADDRESS="/deep";
    private final String deepAddress;
    public RestTemplate restTemplate;
    private String serviceAddress;
    private String classifierAddress;
    private String quantityAddress;

    public SentimentAnalyzersServiceClient(String serviceAddress) {
        this.serviceAddress = serviceAddress;
        restTemplate=new RestTemplate();
        quantityAddress=serviceAddress+QUANTITY_ADDRESS;
        classifierAddress=serviceAddress+ CLASSIFIER_ADDRESS;
        deepAddress = serviceAddress + DEEP_ADDRESS;
    }


    @Override
    public Optional<SentimentAnalyzeResult> quantityAnalyze(String text) {
        return getSentimentAnalyzeResult(text, quantityAddress);
    }
    @Override
    public Optional<SentimentAnalyzeResult> classifierAnalyze(String text) {
        return getSentimentAnalyzeResult(text, classifierAddress);
    }

    @Override
    public Optional<SentimentAnalyzeResult> deepAnalyze(String text) {
        return getSentimentAnalyzeResult(text, deepAddress);
    }

    private Optional<SentimentAnalyzeResult> getSentimentAnalyzeResult(String text, String address) {
        HttpHeaders headers=new HttpHeaders();
        headers.setContentType(MEDIA_TYPE_JSON_UTF_8);
        HttpEntity<String> entity=new HttpEntity<>(text,headers);
        ResponseEntity<SentimentAnalyzeResult> response =  restTemplate.postForEntity(address, entity, SentimentAnalyzeResult.class);
        return Optional.of(response.getBody());
    }

}
