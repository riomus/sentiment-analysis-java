package pl.riomus.sentiment.service.client;

import pl.riomus.nlp.api.model.SentimentAnalyzeResult;

import java.util.Optional;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
public interface SentimentAnalyzers {
    Optional<SentimentAnalyzeResult> quantityAnalyze(String text);
    Optional<SentimentAnalyzeResult> classifierAnalyze(String text);
    Optional<SentimentAnalyzeResult> deepAnalyze(String text);
}
