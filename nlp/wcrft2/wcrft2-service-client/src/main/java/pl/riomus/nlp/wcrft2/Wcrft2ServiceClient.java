package pl.riomus.nlp.wcrft2;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import pl.riomus.nlp.api.CCLParser;
import pl.riomus.nlp.api.ContentTagger;
import pl.riomus.nlp.api.model.TaggedContent;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.nio.charset.Charset;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
public class Wcrft2ServiceClient implements ContentTagger {
    public static final String TAG_SINGLE_SERVICE = "/tag";
    public static final MediaType MEDIA_TYPE_XML_UTF_8 = new MediaType("application", "atom+xml", Charset.forName("UTF-8"));
    private RestTemplate restTemplate;
    private String serviceAddress;
    private CCLParser cclParser;
    private static final String SERVICE_URL_TEMPLATE = "%s/%s";
    private String tagSingleAddress;

    public Wcrft2ServiceClient(String serviceAddress, CCLParser cclParser) {
        this.serviceAddress = serviceAddress;
        this.cclParser = cclParser;
        restTemplate = new RestTemplate();
        tagSingleAddress = String.format(SERVICE_URL_TEMPLATE, serviceAddress, TAG_SINGLE_SERVICE);
    }

    @Override
    public TaggedContent tag(String stringToTag) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MEDIA_TYPE_XML_UTF_8);
        HttpEntity httpEntity = new HttpEntity(stringToTag, httpHeaders);
        String cclString = restTemplate.postForObject(tagSingleAddress, httpEntity, String.class);
        return new TaggedContent(cclParser.parse(cclString));
    }


}