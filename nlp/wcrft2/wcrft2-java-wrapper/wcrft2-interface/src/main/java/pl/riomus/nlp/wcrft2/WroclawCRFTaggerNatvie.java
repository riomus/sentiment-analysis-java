package pl.riomus.nlp.wcrft2;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
public class WroclawCRFTaggerNatvie {

    public List<String> tagStrings(List<String> stringsToTag){
      return Arrays.asList(tagStrings((String[]) stringsToTag.toArray()));
    }

    public native String[] tagStrings(String[] stringsToTag);

}
