#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <jni.h>
#include <wcrft2Java.h>
#include <libwcrft/tagger.h>

JNIEXPORT jobjectArray JNICALL Java_pl_riomus_nlp_wcrft2_WroclawCRFTaggerNatvie_tagStrings(JNIEnv * env, jobject obj, jobjectArray stringArray)
{
    Wcrft::Tagger tagger ("nkjp_e2");
    tagger.load_model();

    int stringCount = env->GetArrayLength(stringArray);
    jobjectArray output=(jobjectArray)env->NewObjectArray(stringCount,env->FindClass("java/lang/String"), NULL);
    for (int i=0; i<stringCount; i++) {
      jstring string = (jstring)env->GetObjectArrayElement( stringArray, i);
      const char *rawString =env->GetStringUTFChars( string, 0);
      std::stringstream inputStream;
      std::stringstream outputStream;
      inputStream<<rawString;
      tagger.tag_input(inputStream,"txt",outputStream,"ccl");
      env->SetObjectArrayElement(output,i,env->NewStringUTF(outputStream.str().c_str()));
      env->ReleaseStringUTFChars(string, rawString);
      env->DeleteLocalRef(string);
  }
  return output;
}

int main()
{
  return 0;
}