package pl.riomus.nlp.wcrft2;

import java.util.Arrays;
import java.util.List;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
public class WroclawCRFTagger extends WroclawCRFTaggerNatvie {
    static {
        System.loadLibrary("Wcrft2Native");
    }
}
