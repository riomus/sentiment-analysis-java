package pl.riomus.nlp.wcrft2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */


@ComponentScan
@EnableAutoConfiguration
public class Wcrft2ServiceApp {


    public static void main(String[] args) throws Exception {
        SpringApplication.run(Wcrft2ServiceApp.class, args);
    }

    @Bean
    public WroclawCRFTagger getTwitterOAuthProvider() {
        return new WroclawCRFTagger();
    }
}
