package pl.riomus.nlp.wcrft2;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.List;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
@RestController
public class Wcrf2Service {

    private WroclawCRFTagger wroclawCRFTagger;

    @Inject
    public Wcrf2Service(WroclawCRFTagger wroclawCRFTagger) {
        this.wroclawCRFTagger = wroclawCRFTagger;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/tagMultiple", produces = "application/atom+xml;charset=UTF-8",consumes = "application/atom+xml")
    public List<String> tag(@RequestBody String[] input) {
        return wroclawCRFTagger.tagStrings(Arrays.asList(input));
    }
    @RequestMapping(method = RequestMethod.POST, value = "/tag", produces = "application/atom+xml;charset=UTF-8",consumes="application/atom+xml")
    public String tag(@RequestBody  String input) {
        return wroclawCRFTagger.tagStrings(Arrays.asList(input)).get(0);
    }

}
