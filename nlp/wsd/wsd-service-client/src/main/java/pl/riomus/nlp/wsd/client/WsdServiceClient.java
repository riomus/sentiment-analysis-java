package pl.riomus.nlp.wsd.client;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import pl.riomus.nlp.api.CCLContentDisambiguator;
import pl.riomus.nlp.api.CCLParser;
import pl.riomus.nlp.api.model.DisambiguatedContent;
import pl.riomus.nlp.api.model.TaggedContent;
import pl.riomus.nlp.api.model.ccl.ChunkList;

import java.nio.charset.Charset;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
public class WsdServiceClient implements CCLContentDisambiguator {

    public static final MediaType MEDIA_TYPE_XML_UTF_8 = new MediaType("application", "atom+xml", Charset.forName("UTF-8"));

    private final RestTemplate restTemplate;
    private String serviceAddress;
    private CCLParser cclParser;

    public WsdServiceClient(String serviceAddress, CCLParser cclParser) {
        this.serviceAddress = serviceAddress;
        this.cclParser = cclParser;
        restTemplate = new RestTemplate();
    }

    @Override
    public DisambiguatedContent disambiguate(TaggedContent taggedContent) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MEDIA_TYPE_XML_UTF_8);
        HttpEntity httpEntity = new HttpEntity(taggedContent.getContent(), httpHeaders);
        String serviceResponse = restTemplate.postForObject(serviceAddress, httpEntity, String.class);
        ChunkList chunkList = cclParser.parse(serviceResponse);
        return new DisambiguatedContent(chunkList);
    }


}
