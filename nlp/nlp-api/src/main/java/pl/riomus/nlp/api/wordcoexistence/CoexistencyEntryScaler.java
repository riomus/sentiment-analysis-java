package pl.riomus.nlp.api.wordcoexistence;

import java.util.List;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
public interface CoexistencyEntryScaler {
    List<CoexistenceEntry> scaleCoexistenceEntries(List<CoexistenceEntry> input, int outputDimension);
}
