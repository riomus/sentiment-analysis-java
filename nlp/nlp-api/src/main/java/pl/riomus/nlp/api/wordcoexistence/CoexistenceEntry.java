package pl.riomus.nlp.api.wordcoexistence;

import java.util.List;

/**
 * Created by Roman Bartusiak (roman.bartusiak@nsn.com)
 */
public class CoexistenceEntry {
    private String word;
    private List<ValueEntry> values;
    private int vectorSize;

    public CoexistenceEntry(String word, List<ValueEntry> values, int vectorSize) {
        this.word = word;
        this.values = values;
        this.vectorSize = vectorSize;
    }

    public CoexistenceEntry() {
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public List<ValueEntry> getValues() {
        return values;
    }

    public void setValues(List<ValueEntry> values) {
        this.values = values;
    }

    public int getVectorSize() {
        return vectorSize;
    }

    public void setVectorSize(int vectorSize) {
        this.vectorSize = vectorSize;
    }
}
