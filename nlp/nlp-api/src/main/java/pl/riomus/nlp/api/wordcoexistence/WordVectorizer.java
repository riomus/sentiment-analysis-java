package pl.riomus.nlp.api.wordcoexistence;

import java.util.Optional;

/**
 * Created by Roman Bartusiak (roman.bartusiak@nsn.com)
 */
public interface WordVectorizer {
     Optional<double[]> vectorize(String word);

    double[] getUnknownVector();
}
