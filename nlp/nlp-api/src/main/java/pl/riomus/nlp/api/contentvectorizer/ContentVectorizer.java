package pl.riomus.nlp.api.contentvectorizer;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
public interface ContentVectorizer {
    double[][] vectorize(String content);
}
