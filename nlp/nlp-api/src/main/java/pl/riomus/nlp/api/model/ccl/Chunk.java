package pl.riomus.nlp.api.model.ccl;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.util.Arrays;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
public class Chunk
{
    private String id;
    private Sentence[] sentences;

    private String type;

    @XmlAttribute(name="id")
    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    @XmlElement(name="sentence")
    public Sentence[] getSentences()
    {
        return sentences;
    }

    public void setSentences(Sentence[] sentences)
    {
        this.sentences = sentences;
    }

    @XmlAttribute
    public String getType ()
    {
        return type;
    }

    public void setType (String type)
    {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Chunk{" +
                "id='" + id + '\'' +
                ", sentences=" + Arrays.toString(sentences) +
                ", type='" + type + '\'' +
                '}';
    }
}
