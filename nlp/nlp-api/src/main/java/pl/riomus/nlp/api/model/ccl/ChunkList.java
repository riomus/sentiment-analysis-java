package pl.riomus.nlp.api.model.ccl;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Arrays;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */

@XmlRootElement
public class ChunkList
{
    private Chunk[] chunks;
    @XmlElement(name="chunk")
    public Chunk[] getChunks()
    {
        return chunks;
    }

    public void setChunks(Chunk[] chunks)
    {
        this.chunks = chunks;
    }

    @Override
    public String toString() {
        return "ChunkList{" +
                "chunks=" + Arrays.toString(chunks) +
                '}';
    }
}


