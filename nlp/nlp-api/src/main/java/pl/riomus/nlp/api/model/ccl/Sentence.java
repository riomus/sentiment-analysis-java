package pl.riomus.nlp.api.model.ccl;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import java.util.Arrays;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
public class Sentence
{
    private String id;

    private String ns;

    private Token[] tokens=new Token[0];
    @XmlAttribute
    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getNs ()
    {
        return ns;
    }

    public void setNs (String ns)
    {
        this.ns = ns;
    }
    @XmlElement(name="tok")
    public Token[] getTokens ()
    {
        return tokens;
    }

    public void setTokens (Token[] tokens)
    {
        this.tokens = tokens;
    }

    @Override
    public String toString() {
        return "Sentence{" +
                "id='" + id + '\'' +
                ", ns='" + ns + '\'' +
                ", tokens=" + Arrays.toString(tokens) +
                '}';
    }
}