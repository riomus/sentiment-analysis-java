package pl.riomus.nlp.api;

import pl.riomus.nlp.api.model.TaggedContent;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
public interface ContentTagger {
    TaggedContent tag(String stringToTag);
}
