package pl.riomus.nlp.api.wordcoexistence;

/**
 * Created by Roman Bartusiak (roman.bartusiak@nsn.com)
 */
public class ValueEntry {
    private int index;
    private double value;

    public ValueEntry(int index, double value) {
        this.index = index;
        this.value = value;
    }

    public ValueEntry() {
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public int getIndex() {
        return index;
    }

    public double getValue() {
        return value;
    }
}
