package pl.riomus.nlp.api.model;

import pl.riomus.nlp.api.model.ccl.ChunkList;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
public class DisambiguatedContent extends CCLContent {


    public DisambiguatedContent(ChunkList content) {
        super(content);
    }
}
