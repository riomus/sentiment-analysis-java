package pl.riomus.nlp.api.model;

import pl.riomus.wordnet.api.models.Sentiment;

import java.util.HashMap;
import java.util.Map;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
public class SentimentAnalyzeResult {
    private Sentiment sentiment;
    private Map<String,Object> metadata=new HashMap<>();
    public Sentiment getSentiment() {
        return sentiment;
    }

    public void setSentiment(Sentiment sentiment) {
        this.sentiment = sentiment;
    }

    public void addMetadata(String key,Object value){
        metadata.put(key,value);
    }

    public Map<String, Object> getMetadata() {
        return metadata;
    }

    public Object getMetadata(String key){
        return key;
    }

    @Override
    public String toString() {
        return "SentimentAnalyzeResult{" +
                "sentiment=" + sentiment +
                ", metadata=" + metadata +
                '}';
    }
}
