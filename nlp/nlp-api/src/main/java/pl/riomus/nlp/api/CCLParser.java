package pl.riomus.nlp.api;

import pl.riomus.nlp.api.model.ccl.ChunkList;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
public interface CCLParser {
    ChunkList parse(String cclString);
    String marshall(ChunkList chunkList);
}
