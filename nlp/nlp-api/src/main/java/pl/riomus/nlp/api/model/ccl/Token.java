package pl.riomus.nlp.api.model.ccl;

import javax.xml.bind.annotation.XmlElement;
import java.util.Arrays;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
public class Token {
    private LexicalUnit lexicalUnit;
    private String orth;
    private Property[] properties = new Property[0];

    @XmlElement(name = "lex")
    public LexicalUnit getLexicalUnit() {
        return lexicalUnit;
    }

    public void setLexicalUnit(LexicalUnit lexicalUnit) {
        this.lexicalUnit = lexicalUnit;
    }

    public String getOrth() {
        return orth;
    }

    public void setOrth(String orth) {
        this.orth = orth;
    }

    @XmlElement(name = "prop")
    public Property[] getProperties() {
        return properties;
    }

    public void setProperties(Property[] properties) {
        this.properties = properties;
    }

    @Override
    public String toString() {
        return "Tok{" +
                "lexicalUnit=" + lexicalUnit +
                ", orth='" + orth + '\'' +
                ", properties=" + Arrays.toString(properties) +
                '}';
    }
}
