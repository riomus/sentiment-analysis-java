package pl.riomus.nlp.api.wordcoexistence;

import java.util.Optional;
import java.util.Set;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
public interface CoexistencyEntryProvider {
    Optional<CoexistenceEntry> getCoexistencyEntry(String word);
    Set<String> getAllWords();
    int getCoexsistenctVectorSize();
}
