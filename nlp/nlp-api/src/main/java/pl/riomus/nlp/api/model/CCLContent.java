package pl.riomus.nlp.api.model;

import pl.riomus.nlp.api.model.ccl.ChunkList;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
public abstract class CCLContent {
    private ChunkList content;

    public ChunkList getContent() {
        return content;
    }

    CCLContent(ChunkList content) {
        this.content = content;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CCLContent that = (CCLContent) o;

        if (content != null ? !content.equals(that.content) : that.content != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return content != null ? content.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "StringContent{" +
                "content='" + content + '\'' +
                '}';
    }
}
