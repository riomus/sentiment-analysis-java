package pl.riomus.nlp.api.model.ccl;

import javax.xml.bind.annotation.XmlAttribute;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
public class LexicalUnit
{
    private String disamb;

    private String ctag;

    private String base;

    @XmlAttribute
    public String getDisamb ()
    {
        return disamb;
    }

    public void setDisamb (String disamb)
    {
        this.disamb = disamb;
    }

    public String getCtag ()
    {
        return ctag;
    }

    public void setCtag (String ctag)
    {
        this.ctag = ctag;
    }

    public String getBase ()
    {
        return base;
    }

    public void setBase (String base)
    {
        this.base = base;
    }

    @Override
    public String toString() {
        return "Lex{" +
                "disamb='" + disamb + '\'' +
                ", ctag='" + ctag + '\'' +
                ", base='" + base + '\'' +
                '}';
    }
}

