package pl.riomus.nlp.api.sentimentanalyzers;

import pl.riomus.nlp.api.model.SentimentAnalyzeResult;

/**
 * Created by Roman Bartusiak (roman.bartusiak@nsn.com)
 */
public interface SentimentAnalyzer {
    SentimentAnalyzeResult analyze(String content);
}
