package pl.riomus.nlp.api.model.ccl;


import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlValue;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
public class Property
{
    private String value;

    private String key;

    @XmlValue
    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    @XmlAttribute
    public String getKey ()
    {
        return key;
    }

    public void setKey (String key)
    {
        this.key = key;
    }

    @Override
    public String toString() {
        return "Prop{" +
                "value='" + value + '\'' +
                ", key='" + key + '\'' +
                '}';
    }
}
