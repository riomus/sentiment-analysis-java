package pl.riomus.nlp.ccl.parser;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
public class CCLContentUnmarshalerException extends RuntimeException {
    public CCLContentUnmarshalerException(String s, Exception e) {
        super(s,e);
    }
}
