package pl.riomus.nlp.ccl.parser;

import pl.riomus.nlp.api.CCLParser;
import pl.riomus.nlp.api.model.DisambiguatedContent;
import pl.riomus.nlp.api.model.ccl.*;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
public class BasicCCLParser implements CCLParser{

    private CCLContentUnmarshaler cclContentUnmarshaler;

    public BasicCCLParser() {
        this(new  CCLContentUnmarshaler());
    }

     BasicCCLParser(CCLContentUnmarshaler cclContentUnmarshaler) {
         this.cclContentUnmarshaler = cclContentUnmarshaler;
     }


    public String marshall(ChunkList chunkList){
        return cclContentUnmarshaler.marshall(chunkList);
    }
    public ChunkList parse(String cclString){
        return cclContentUnmarshaler.unmarshal(cclString);
    }

    public static void main(String[] args) {
        BasicCCLParser cclParser = new BasicCCLParser();
        ChunkList synsetIdentifiers = cclParser.parse("<?xml version=\"1.0\" encoding=\"UTF-8\"?>  \n" +
                "<!DOCTYPE chunkList SYSTEM \"ccl.dtd\">  \n" +
                "<chunkList>  \n" +
                " <chunk id=\"ch1\" type=\"p\">  \n" +
                "  <sentence id=\"s1\">  \n" +
                "   <tok>  \n" +
                "    <orth>Ala</orth>  \n" +
                "    <lex disamb=\"1\"><base>Ala</base><ctag>subst:sg:nom:f</ctag></lex>  \n" +
                "    <prop key=\"sense:ukb:syns_id\">236786</prop>  \n" +
                "    <prop key=\"sense:ukb:syns_rank\">236786/1</prop>  \n" +
                "    <prop key=\"sense:ukb:unitsstr\">ala-2(grp)</prop>  \n" +
                "   </tok>  \n" +
                "   <tok>  \n" +
                "    <orth>ma</orth>  \n" +
                "    <lex disamb=\"1\"><base>mieć</base><ctag>fin:sg:ter:imperf</ctag></lex>  \n" +
                "    <prop key=\"sense:ukb:syns_id\">8913</prop>  \n" +
                "    <prop key=\"sense:ukb:syns_rank\">8913/0.692342 59906/0.222411 55321/0.0635641 1719/0.0216837</prop>  \n" +
                "    <prop key=\"sense:ukb:unitsstr\">być_w_posiadaniu-1(cpos) posiadać-2(cpos) mieć-3(cpos)</prop>  \n" +
                "   </tok>  \n" +
                "   <tok>  \n" +
                "    <orth>kota</orth>  \n" +
                "    <lex disamb=\"1\"><base>kot</base><ctag>subst:sg:acc:m2</ctag></lex>  \n" +
                "    <prop key=\"sense:ukb:syns_id\">34154</prop>  \n" +
                "    <prop key=\"sense:ukb:syns_rank\">34154/0.678118 227074/0.182792 5168/0.119853 227075/0.0110984 406482/0.00383561 75697/0.0033775 405249/0.000926005</prop>  \n" +
                "    <prop key=\"sense:ukb:unitsstr\">kot-2(zw) kot_domowy-2(zw)</prop>  \n" +
                "   </tok>  \n" +
                "   <ns/>  \n" +
                "   <tok>  \n" +
                "    <orth>.</orth>  \n" +
                "    <lex disamb=\"1\"><base>.</base><ctag>interp</ctag></lex>  \n" +
                "   </tok>  \n" +
                "  </sentence>  \n" +
                " </chunk>  \n" +
                "</chunkList> ");
        System.out.println(synsetIdentifiers);
    }
}
