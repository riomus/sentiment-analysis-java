package pl.riomus.nlp.ccl.parser;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;
import pl.riomus.nlp.api.model.ccl.ChunkList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.sax.SAXSource;
import java.io.Serializable;
import java.io.StringReader;
import java.io.StringWriter;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
public class CCLContentUnmarshaler{

    public ChunkList unmarshal(String content) {
        try {
            return getCclContent(content);
        } catch (Exception e) {
            throw new CCLContentUnmarshalerException("Failed to unmarshall file!", e);
        }
    }

    private ChunkList getCclContent(String content) throws Exception {
        JAXBContext jc = JAXBContext.newInstance(ChunkList.class);

        SAXParserFactory spf = SAXParserFactory.newInstance();
        spf.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
        spf.setFeature("http://xml.org/sax/features/validation", false);

        XMLReader xmlReader = spf.newSAXParser().getXMLReader();
        InputSource inputSource = new InputSource(
                new StringReader(content));
        SAXSource source = new SAXSource(xmlReader, inputSource);

        Unmarshaller unmarshaller = jc.createUnmarshaller();
        return (ChunkList) unmarshaller.unmarshal(source);
    }

    public String marshall(ChunkList chunkList) {

        try {
            return marshalToXml(chunkList);
        } catch (Exception e) {
            throw new CCLContentUnmarshalerException("Failed to marshall file!", e);
        }
    }

    private String marshalToXml(ChunkList chunkList) throws JAXBException {
        JAXBContext jc = JAXBContext.newInstance(ChunkList.class);
        StringWriter stringWriter = new StringWriter();
        Marshaller marshaller = jc.createMarshaller();
        marshaller.marshal(chunkList,stringWriter);
        return stringWriter.toString();
    }
}

