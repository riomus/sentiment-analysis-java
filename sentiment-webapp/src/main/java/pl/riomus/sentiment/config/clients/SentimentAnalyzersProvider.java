package pl.riomus.sentiment.config.clients;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.riomus.sentiment.service.client.SentimentAnalyzers;
import pl.riomus.sentiment.service.client.SentimentAnalyzersServiceClient;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
@Configuration
public class SentimentAnalyzersProvider {

    public static final String DEFAULT_SERVICE_ADDRESS = "http://sentimentservice:8080";
    public static final String SENTIMENT_SERVICE_ADDRESS_PROPERTY = "pl.riomus.sentiment.analyzers.address";

    @Bean
    public SentimentAnalyzers createSentimentAnalyzers(){
        return new SentimentAnalyzersServiceClient(System.getProperty(SENTIMENT_SERVICE_ADDRESS_PROPERTY, DEFAULT_SERVICE_ADDRESS));
    }
}
