package pl.riomus.sentiment.service;

import org.springframework.stereotype.Service;
import pl.riomus.nlp.api.model.SentimentAnalyzeResult;
import pl.riomus.sentiment.service.client.SentimentAnalyzers;

import javax.inject.Inject;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
@Service
public class SentimentAnalyzersService {

    private SentimentAnalyzers sentimentAnalyzers;

    @Inject
    public SentimentAnalyzersService(SentimentAnalyzers sentimentAnalyzers) {
        this.sentimentAnalyzers = sentimentAnalyzers;
    }

    public SentimentAnalyzeResult quantityAnalyze(String text){
        return sentimentAnalyzers.quantityAnalyze(text).get();
    }
    public SentimentAnalyzeResult classifierAnalyze(String text){
        return sentimentAnalyzers.classifierAnalyze(text).get();
    }
    public SentimentAnalyzeResult deepAnalyze(String text){
        return sentimentAnalyzers.deepAnalyze(text).get();
    }
}
