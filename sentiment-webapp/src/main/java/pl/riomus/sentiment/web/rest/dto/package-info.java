/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package pl.riomus.sentiment.web.rest.dto;
