package pl.riomus.sentiment.web.rest;

import com.codahale.metrics.annotation.Timed;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import pl.riomus.nlp.api.model.SentimentAnalyzeResult;
import pl.riomus.sentiment.security.AuthoritiesConstants;
import pl.riomus.sentiment.service.SentimentAnalyzersService;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
@RestController
@RequestMapping("/app/rest/sentiment")
public class SentimentResource {
    private SentimentAnalyzersService sentimentAnalyzersService;

    @Inject
    public SentimentResource(SentimentAnalyzersService sentimentAnalyzersService) {
        this.sentimentAnalyzersService = sentimentAnalyzersService;
    }

    @Timed
    @RequestMapping(value = "/quantity",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public SentimentAnalyzeResult quantityAnalyze(@RequestBody String text) {
        System.out.println(text);
        return sentimentAnalyzersService.quantityAnalyze(text);
    }

    @Timed
    @RequestMapping(value = "/classifier",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public SentimentAnalyzeResult classfierAnalyze(@RequestBody String text) {
        System.out.println(text);
        return sentimentAnalyzersService.classifierAnalyze(text);
    }
    @Timed
    @RequestMapping(value = "/deep",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    public SentimentAnalyzeResult deepAnalyze(@RequestBody String text) {
        System.out.println(text);
        return sentimentAnalyzersService.deepAnalyze(text);
    }
}
