package pl.riomus.allegro.data.service;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import pl.riomus.allegro.data.entity.AllegroCommentEntity;
import pl.riomus.allegro.data.repositories.multiple.AllegroCommentsPersister;

import javax.inject.Inject;
import java.util.List;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
@RestController
public class AllegroMultipleCommentsCreationEndpoint {

    @Inject
    private AllegroCommentsPersister allegroComentsPersister;


    @RequestMapping(method = RequestMethod.POST, value = "/allegroComments")
    public void createComments(@RequestBody List<AllegroCommentEntity> comments) {
        allegroComentsPersister.persistComments(comments);
    }



}
