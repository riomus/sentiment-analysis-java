package pl.riomus.allegro.data.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import pl.riomus.allegro.data.entity.AllegroCommentEntity;

import java.util.List;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
@RepositoryRestResource(collectionResourceRel = "allegroComments", path = "allegroComment")
public interface AllegroCommentRepository extends PagingAndSortingRepository<AllegroCommentEntity, Integer> {
  @Query(value="select * from allegroComment WHERE type=:type LIMIT :from,:to",nativeQuery = true)
   //@Query(value="select * from allegroComment WHERE type=:type",nativeQuery = true)
    List<AllegroCommentEntity> findByType(@Param("type")String type,@Param("from")int from,@Param("to") int to);
}
