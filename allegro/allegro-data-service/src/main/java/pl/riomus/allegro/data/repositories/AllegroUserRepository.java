package pl.riomus.allegro.data.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import pl.riomus.allegro.data.entity.AllegroUserEntity;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
@RepositoryRestResource(collectionResourceRel = "allegroUsers", path = "allegroUser")
public interface AllegroUserRepository extends PagingAndSortingRepository<AllegroUserEntity, Integer> {
}
