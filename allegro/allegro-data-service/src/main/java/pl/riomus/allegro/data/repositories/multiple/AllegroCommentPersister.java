package pl.riomus.allegro.data.repositories.multiple;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import pl.riomus.allegro.data.entity.AllegroCommentEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
@Component
public class AllegroCommentPersister {
    private static final Logger LOG = Logger.getLogger(AllegroCommentPersister.class);

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional(propagation= Propagation.REQUIRES_NEW)
    public void persistComments(AllegroCommentEntity comment) {
        entityManager.persist(comment);
    }


}
