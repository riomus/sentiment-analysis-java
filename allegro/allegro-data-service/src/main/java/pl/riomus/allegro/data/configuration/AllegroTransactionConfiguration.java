package pl.riomus.allegro.data.configuration;

import org.springframework.context.annotation.Configuration;
import pl.riomus.spring.rest.coniguration.TransactionConfiguration;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
@Configuration
public class AllegroTransactionConfiguration extends TransactionConfiguration {
}
