package pl.riomus.allegro.data.repositories.multiple;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import pl.riomus.allegro.data.entity.AllegroCommentEntity;

import javax.inject.Inject;
import java.util.List;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
@Component
public class AllegroCommentsPersister {
    private static final Logger LOG = Logger.getLogger(AllegroCommentsPersister.class);

    @Inject
    private AllegroCommentPersister allegroCommentPersister;

    public void persistComments(List<AllegroCommentEntity> comments) {
        LOG.info("Persisting comments "+comments.size());
        comments.parallelStream().forEach(this::persistComment);
        LOG.info("Persisted comments "+comments.size());
    }



    private void persistComment(AllegroCommentEntity comment){
        try{
           allegroCommentPersister.persistComments(comment);
        }
        catch(Exception e){
            LOG.error("Failed to persist comment"+comment.getId());
        }

    }


}
