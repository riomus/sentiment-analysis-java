package pl.riomus.allegro.data.comment;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
public class AllegroComment {
    private int id;
    private int authorId;
    private int reciverId;
    private String authorLogin;
    private AllegroCommentType type;
    private String content;


    public AllegroComment() {
    }

    public AllegroComment(int id, int authorId, int reciverId, AllegroCommentType type, String content, String authorLogin) {
        this.id = id;
        this.authorId = authorId;
        this.reciverId = reciverId;
        this.type = type;
        this.content = content;
    }

    public int getId() {
        return id;
    }

    public int getAuthorId() {
        return authorId;
    }

    public int getReciverId() {
        return reciverId;
    }

    public AllegroCommentType getType() {
        return type;
    }

    public String getContent() {
        return content;
    }

    public String getAuthorLogin() {
        return authorLogin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AllegroComment that = (AllegroComment) o;

        if (id != that.id) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public String toString() {
        return "AllegroComment{" +
                "id=" + id +
                ", authorId=" + authorId +
                ", reciverId=" + reciverId +
                ", type=" + type +
                ", content='" + content + '\'' +
                '}';
    }
}
