package pl.riomus.allegro.data.comment;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
public enum AllegroCommentType {
    POS, NEG, NEU, UNKNOWN;
}
