package pl.riomus.allegro.client;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
public class AllegroAuthenticationData {
    private String userLogin;
    private String userPassword;
    private String webApiKey;
    private int countryKey;

    public AllegroAuthenticationData(String userLogin, String userPassword, String webApiKey, int countryKey) {
        this.userLogin = userLogin;
        this.userPassword = userPassword;
        this.webApiKey = webApiKey;
        this.countryKey = countryKey;
    }

    public String getUserLogin() {
        return userLogin;
    }


    public String getUserPassword() {
        return userPassword;
    }


    public String getWebApiKey() {
        return webApiKey;
    }

    public int getCountryKey() {
        return countryKey;
    }
}
