package pl.riomus.allegro.client;


import pl.riomus.allegro.client.generated.*;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
public abstract class AllegroClient {

    protected ServicePort allegroServicePort;
    protected String sessionHandle;
    protected long localVersion;
    protected AllegroAuthenticationData authenticationData;

    public AllegroClient(AllegroAuthenticationData authenticationData) {
        this.authenticationData = authenticationData;
        try {
            createAllegroServiceClient();
        } catch (Exception e) {
            throw new AllegroClientException("Failed to create Allegro WebAPI client!", e);
        }
        initalizeLocalVersion(authenticationData);
        loginToAllegroService(authenticationData);
    }

    protected void initalizeLocalVersion(AllegroAuthenticationData authenticationData) {
        DoQuerySysStatusRequest doQuerySysStatusRequest = new DoQuerySysStatusRequest();
        doQuerySysStatusRequest.setCountryId(authenticationData.getCountryKey());
        doQuerySysStatusRequest.setSysvar(3);
        doQuerySysStatusRequest.setWebapiKey(authenticationData.getWebApiKey());
        DoQuerySysStatusResponse doQuerySysStatusResponse = allegroServicePort.doQuerySysStatus(doQuerySysStatusRequest);
        this.localVersion = doQuerySysStatusResponse.getVerKey();
    }

    protected void loginToAllegroService(AllegroAuthenticationData authenticationData) {
        DoLoginRequest doLoginRequest = new DoLoginRequest();
        doLoginRequest.setUserLogin(authenticationData.getUserLogin());
        doLoginRequest.setUserPassword(authenticationData.getUserPassword());
        doLoginRequest.setWebapiKey(authenticationData.getWebApiKey());
        doLoginRequest.setCountryCode(authenticationData.getCountryKey());
        doLoginRequest.setLocalVersion(localVersion);
        DoLoginResponse doLoginResponse = allegroServicePort.doLogin(doLoginRequest);

        if (doLoginResponse.getSessionHandlePart().isEmpty()) {
            throw new AllegroClientException("Failed to authenticate in Allegro WebAPI!");
        } else {
            this.sessionHandle = doLoginResponse.getSessionHandlePart();
        }
    }

    private void createAllegroServiceClient() throws Exception {
        ServiceService allegroService = new ServiceService();
        allegroServicePort = allegroService.getServicePort();
    }
}
