package pl.riomus.allegro.client;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
public class AllegroClientException extends RuntimeException {
    public AllegroClientException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public AllegroClientException(String s) {
        super(s);
    }
}
