package pl.riomus.allegro.comment;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
public class AllegroDataProviderException extends RuntimeException {

    public AllegroDataProviderException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public AllegroDataProviderException(String s) {
        super(s);
    }
}
