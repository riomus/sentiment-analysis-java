package pl.riomus.allegro.comment;

import pl.riomus.allegro.client.generated.FeedbackList;
import pl.riomus.allegro.data.comment.AllegroComment;
import pl.riomus.allegro.data.comment.AllegroCommentType;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
public class AllegroCommentTransformer {

    public AllegroComment fromFeedbackList(FeedbackList feedbackList) {
        AllegroCommentType type = feedbackList.getFType().isEmpty() ? AllegroCommentType.UNKNOWN : AllegroCommentType.valueOf(feedbackList.getFType());
        return new AllegroComment(
                feedbackList.getFId(),
                feedbackList.getFFromId(),
                feedbackList.getFToId(),
                type,
                feedbackList.getFDesc(),
                feedbackList.getFUserLogin());
    }
}
