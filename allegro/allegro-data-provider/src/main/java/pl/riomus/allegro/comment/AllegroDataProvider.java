package pl.riomus.allegro.comment;


import pl.riomus.allegro.client.AllegroAuthenticationData;
import pl.riomus.allegro.client.AllegroClient;
import pl.riomus.allegro.client.generated.DoGetFeedbackRequest;
import pl.riomus.allegro.client.generated.DoGetFeedbackResponse;
import pl.riomus.allegro.client.generated.DoShowUserRequest;
import pl.riomus.allegro.client.generated.DoShowUserResponse;
import pl.riomus.allegro.data.comment.AllegroComment;
import pl.riomus.allegro.data.user.AllegroUser;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
public class AllegroDataProvider extends AllegroClient {
    private static final Logger LOG = Logger.getLogger(" pl.riomus.allegro.comment.AllegroDataProvider");

    private final AllegroCommentTransformer allegroCommentTransformer;

    public AllegroDataProvider(AllegroAuthenticationData authenticationData) {
        super(authenticationData);
        allegroCommentTransformer = new AllegroCommentTransformer();
    }

    public AllegroUser getUser(String userLogin) {
        DoShowUserRequest doShowUserRequest = createBasicDoShowUserRequest();
        doShowUserRequest.setUserLogin(userLogin);
        DoShowUserResponse doShowUserResponse = allegroServicePort.doShowUser(doShowUserRequest);

        AllegroUser out = new AllegroUser();
        Long userId = doShowUserResponse.getUserId();
        out.setUserId(userId.intValue());
        out.setUserLogin(doShowUserResponse.getUserLogin());
        return out;
    }

    public AllegroUser getUser(Integer userId) {
        DoShowUserRequest doShowUserRequest = createBasicDoShowUserRequest();
        doShowUserRequest.setUserId(userId.longValue());
        DoShowUserResponse doShowUserResponse = allegroServicePort.doShowUser(doShowUserRequest);

        AllegroUser out = new AllegroUser();
        out.setUserId(userId);
        out.setUserLogin(doShowUserResponse.getUserLogin());
        return out;
    }

    private DoShowUserRequest createBasicDoShowUserRequest() {
        DoShowUserRequest doShowUserRequest = new DoShowUserRequest();
        doShowUserRequest.setWebapiKey(authenticationData.getWebApiKey());
        doShowUserRequest.setCountryId(authenticationData.getCountryKey());
        return doShowUserRequest;
    }

    public List<AllegroComment> getUserComments(Integer userId) {
        LOG.info("Fetching comments for user with id " + userId);
        List<AllegroComment> out = new LinkedList<>();
        Integer offset = 0;
        List<AllegroComment> feedbacksPartial = getUserComments(userId, offset);
        out.addAll(feedbacksPartial);
        while (feedbacksPartial.size() == 25) {
            offset += 25;
            feedbacksPartial = getUserComments(userId, offset);
            out.addAll(feedbacksPartial);
        }
        return out;
    }

    public List<AllegroComment> getUserComments(Integer userId, Integer offset) {
            return getUserCommentsWithRetrys(userId, offset,1);
    }

    private List<AllegroComment> getUserCommentsWithRetrys(Integer userId, Integer offset,Integer retrysNumber) {
        try {
        LOG.info("Fetching comments for user with id " + userId + " and offset " + offset);
        DoGetFeedbackRequest doGetFeedbackRequest = new DoGetFeedbackRequest();
        doGetFeedbackRequest.setSessionHandle(sessionHandle);
        doGetFeedbackRequest.setFeedbackTo(userId);
        doGetFeedbackRequest.setFeedbackOffset(offset);
        DoGetFeedbackResponse doGetFeedbackResponse = allegroServicePort.doGetFeedback(doGetFeedbackRequest);
        return doGetFeedbackResponse.getFeedbackList().getItem().stream().parallel().map(allegroCommentTransformer::fromFeedbackList).collect(Collectors.toList());
        }catch (Exception e){
            LOG.severe("Failed to fetch user comments");
            if(retrysNumber>=1) {
                LOG.severe("Trying to relogin, recreate local versions and repeat");
                this.initalizeLocalVersion(authenticationData);
                this.loginToAllegroService(authenticationData);
                return getUserCommentsWithRetrys(userId, offset,--retrysNumber);
            }
            else{
                throw e;
            }
        }
    }
}
