package pl.riomus.allegro.data.client;

import com.google.common.collect.ImmutableMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.PagedResources;
import org.springframework.http.HttpMethod;
import pl.riomus.allegro.data.comment.AllegroComment;
import pl.riomus.allegro.data.comment.AllegroCommentType;
import pl.riomus.allegro.data.user.AllegroUser;
import pl.riomus.spring.repository.client.PaginableRepositoryClient;

import java.util.List;
import java.util.Optional;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
public class AllegroDataServiceClient extends PaginableRepositoryClient {
    private static final Logger LOG = LoggerFactory.getLogger(AllegroDataServiceClient.class);
    private static final String PAGE_URL_PARAMETER = "page";
    private static final String SIZE_URL_PARAMETER = "size";
    private static final String TYPE_URL_PARAMETER = "type";
    private static final String FROM_URL_PARAMETER = "from";
    private static final String COMMENTS_BY_TYPE_PAGE_SIZE_URL_PARAMETER = "to";

    private static final String FIND_BY_TYPE_URL_PART= "/search/findByType?type={type}&from={from}&to={to}";
    private static final String ALLEGRO_COMMENT_URL_PART = "/allegroComment";
    private static final String ALLEGRO_COMMENTS_URL_PART = "/allegroComments";
    private static final String ALLEGRO_USER_URL_PART = "/allegroUser";
    private final String allegroUserUrl;
    private final String allegroCommentUrl;
    private final String allegroCommentsUrl;
    private final String allegroConcretUserUrlTemplate;
    private final String allegroFindByTypeUrl;

    public AllegroDataServiceClient(String repositoryAddress) {
        super(repositoryAddress);
        allegroCommentUrl = repositoryAddress + ALLEGRO_COMMENT_URL_PART;
        allegroUserUrl = repositoryAddress + ALLEGRO_USER_URL_PART;
        allegroCommentsUrl = repositoryAddress + ALLEGRO_COMMENTS_URL_PART;
        allegroConcretUserUrlTemplate = allegroUserUrl + "/%d";
        allegroFindByTypeUrl=allegroCommentUrl+FIND_BY_TYPE_URL_PART;
    }

    public PagedResources<AllegroComment> getComments(int page,int size) {
        return restTemplate.exchange(allegroCommentUrl, HttpMethod.GET, null, new ParameterizedTypeReference<PagedResources<AllegroComment>>() {
        }, ImmutableMap.of(PAGE_URL_PARAMETER, page,SIZE_URL_PARAMETER,size)).getBody();
    }

    public PagedResources<AllegroComment> getComments(AllegroCommentType commentType,int page,int size) {
        return getCommentsFromRange(commentType, page * size, size);
    }


    public PagedResources<AllegroComment> getCommentsFromRange(AllegroCommentType commentType,int from,int size) {
        return restTemplate.exchange(allegroFindByTypeUrl, HttpMethod.GET, null, new ParameterizedTypeReference<PagedResources<AllegroComment>>() {
        }, ImmutableMap.of(FROM_URL_PARAMETER, from, COMMENTS_BY_TYPE_PAGE_SIZE_URL_PARAMETER,size,TYPE_URL_PARAMETER,commentType.toString())).getBody();
    }


    public void createAllegroUser(AllegroUser user) {
        LOG.info("Creating allegro user " + user.toString());
        restTemplate.postForLocation(allegroUserUrl, user);
    }

    public void createAllegroComment(AllegroComment comment) {
        restTemplate.postForLocation(allegroCommentUrl, comment);
    }

    public void createAllegroComments(List<AllegroComment> comments) {
        LOG.info("Creating allegro comments " + comments.size());
        restTemplate.postForLocation(allegroCommentsUrl, comments);
    }

    public Optional<AllegroUser> getUser(int userId) {
        String requestAddress = String.format(allegroConcretUserUrlTemplate, userId);
        Optional<AllegroUser> allegroUserOptional;
        try {
            allegroUserOptional = Optional.ofNullable(restTemplate.getForObject(requestAddress, AllegroUser.class));
        } catch (Exception e) {
            allegroUserOptional = Optional.empty();
        }
        if (allegroUserOptional.isPresent()) {
            allegroUserOptional.get().setUserId(userId);
        }
        return allegroUserOptional;
    }

}
