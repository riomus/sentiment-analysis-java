package pl.riomus.allegro.comment.crawler.api;

import java.util.List;
import java.util.Queue;
import java.util.Set;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
public interface CommentCrawlingService {
    void commentsForUserExtracted();

    void addUserToQueue(Integer user);

    void startCrawling(Integer userIde);
    void startCrawling(List<Integer> userIds);
    void addUserToFailedQueue(Integer user);

    Set<Integer> getUsersSet();
}
