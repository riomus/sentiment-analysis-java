package pl.riomus.allegro.comment.crawler;

import com.google.common.collect.Lists;
import org.apache.log4j.Logger;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import pl.riomus.allegro.comment.AllegroDataProvider;
import pl.riomus.allegro.comment.crawler.api.CommentCrawlingService;
import pl.riomus.allegro.comment.crawler.api.CommentCrawlingTask;
import pl.riomus.allegro.data.client.AllegroDataServiceClient;
import pl.riomus.allegro.data.comment.AllegroComment;
import pl.riomus.allegro.data.user.AllegroUser;

import javax.inject.Inject;
import java.util.List;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
@Component
public class AllegroCommentCrawlingTask implements CommentCrawlingTask {
    private static final Logger LOG = Logger.getLogger(AllegroCommentCrawlingTask.class);
    private CommentCrawlingService commentCrawlingService;
    @Inject
    private AllegroDataProvider allegroDataProvider;
    @Inject
    private AllegroDataServiceClient allegroDataServiceClient;

    public void setCommentCrawlingService(CommentCrawlingService commentCrawlingService) {
        this.commentCrawlingService = commentCrawlingService;
    }

    @Override
    @Async
    public void extractComments(int userId) {
        if(!allegroDataServiceClient.getUser(userId).isPresent()) {
            try {
            extractCommentsForUser(userId);
            }catch(Exception e){
                LOG.error("Failed to crawl for user comments!",e);
                commentCrawlingService.addUserToFailedQueue(userId);
            }
        }else{
            LOG.info("Not crawling for user "+userId+" data already fetched");
        }
        commentCrawlingService.commentsForUserExtracted();
    }

    private void extractCommentsForUser(int userId) {
        LOG.info("Crawling comments of user with id " + userId);
        List<AllegroComment> userComments = allegroDataProvider.getUserComments(userId);
        LOG.info("Recived " + userComments.size() + " comments");
        userComments.stream().forEach(c -> commentCrawlingService.addUserToQueue(c.getAuthorId()));
        LOG.info("Recived users added to crawling queue");
        partailAndPersistCommentsInService(userComments);
        LOG.info("Comments persisted in data service");
        AllegroUser user = allegroDataProvider.getUser(userId);
        allegroDataServiceClient.createAllegroUser(user);
        LOG.info("Crawling for user " + userId + " finished");
    }

    private void partailAndPersistCommentsInService(List<AllegroComment> userComments) {
        Lists.partition(userComments,100000).parallelStream().forEach(this::persistsCommentsInService);
    }

    private void persistsCommentsInService(List<AllegroComment> userComments) {
        try {
            allegroDataServiceClient.createAllegroComments(userComments);
        }catch(Exception e){
            LOG.error("Failed to persits comments in service!",e);
        }
    }

}
