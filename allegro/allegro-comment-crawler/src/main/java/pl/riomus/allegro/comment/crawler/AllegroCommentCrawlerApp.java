package pl.riomus.allegro.comment.crawler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
@ComponentScan("pl.riomus.allegro.comment.crawler")
@EnableAutoConfiguration
@EnableAsync
@EnableScheduling
public class AllegroCommentCrawlerApp {
    public static void main(String[] args) {
        SpringApplication.run(AllegroCommentCrawlerApp.class, args);
    }
}
