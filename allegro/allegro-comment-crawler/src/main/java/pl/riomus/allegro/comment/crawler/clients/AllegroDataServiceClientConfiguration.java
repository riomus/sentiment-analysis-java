package pl.riomus.allegro.comment.crawler.clients;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import pl.riomus.allegro.data.client.AllegroDataServiceClient;

import javax.inject.Inject;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */

@Configuration
@PropertySource("classpath:allegroDataService.properties")
public class AllegroDataServiceClientConfiguration {
    private static final String ALLEGRO_DATA_SERVICE_URL_PROPERTY = "allegro.data.service.url";
    @Inject
    private Environment environment;

    @Bean
    public AllegroDataServiceClient getAllegroDataServiceClient() {
        return new AllegroDataServiceClient(System.getProperty(ALLEGRO_DATA_SERVICE_URL_PROPERTY, environment.getProperty(ALLEGRO_DATA_SERVICE_URL_PROPERTY)));
    }
}
