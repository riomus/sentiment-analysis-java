package pl.riomus.allegro.comment.crawler;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import pl.riomus.allegro.comment.crawler.api.CommentCrawlingService;
import pl.riomus.allegro.comment.crawler.api.CommentCrawlingTask;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.*;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
@Service
public class AllegroCommentCrawlingService implements CommentCrawlingService {
    private TreeSet<Integer> usersSet;
    private TreeSet<Integer> failedUsers;

    @Inject
    private CommentCrawlingTask commentCrawlingTask;

    @PostConstruct
    public void setUp() {
        commentCrawlingTask.setCommentCrawlingService(this);
    }


    @Override
    public void commentsForUserExtracted() {
        if (usersSet.size() > 0) {
            commentCrawlingTask.extractComments(usersSet.pollFirst());
        }
        if(usersSet.size()==0&&failedUsers.size()>0){
            usersSet.addAll(failedUsers);
            failedUsers.clear();
            commentsForUserExtracted();
        }
    }

    @Override
    public void addUserToQueue(Integer user) {
        usersSet.add(user);
    }
    @Override
    public void addUserToFailedQueue(Integer user) {
        failedUsers.add(user);
    }

    @Override
    @Async
    public void startCrawling(Integer userId) {
        usersSet = new TreeSet<>();
        failedUsers=new TreeSet<>();
        commentCrawlingTask.extractComments(userId);
    }

    @Override
    @Async
    public void startCrawling(List<Integer> userIds) {
        usersSet = new TreeSet<>();
        failedUsers=new TreeSet<>();
        usersSet.addAll(userIds);
        commentsForUserExtracted();
    }

    @Override
    public Set<Integer> getUsersSet() {
        return usersSet;
    }
}
