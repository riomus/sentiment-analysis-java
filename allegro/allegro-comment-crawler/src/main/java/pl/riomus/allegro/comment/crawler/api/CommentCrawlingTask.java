package pl.riomus.allegro.comment.crawler.api;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
public interface CommentCrawlingTask {

    void extractComments(int userId);

    void setCommentCrawlingService(CommentCrawlingService commentCrawlingService);
}
