package pl.riomus.allegro.comment.crawler.services;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.*;
import pl.riomus.allegro.comment.AllegroDataProvider;
import pl.riomus.allegro.comment.crawler.api.CommentCrawlingService;
import pl.riomus.allegro.data.user.AllegroUser;

import javax.inject.Inject;
import java.util.Collection;
import java.util.List;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
@RestController
public class AllegroCommentCrawlerEndpoint {

    private static final Logger LOG = Logger.getLogger(AllegroCommentCrawlerEndpoint.class);

    @Inject
    private CommentCrawlingService commentCrawlingService;

    @Inject
    private AllegroDataProvider allegroDataProvider;

    @RequestMapping("/start")
    public String startCrawling(@RequestParam("login") String userLogin) {
        LOG.info("Starting crawling for:" + userLogin);
        AllegroUser user = allegroDataProvider.getUser(userLogin);
        commentCrawlingService.startCrawling(user.getUserId());
        String infoMessage = "Crawling started for user with id " + user.getUserId() + " and login " + userLogin;
        LOG.info(infoMessage);
        return infoMessage;
    }

    @RequestMapping(method = RequestMethod.POST,value="/start")
    public String startCrawling(@RequestBody List<Integer> userIds) {
        LOG.info("Starting crawling for queue " + userIds);
        commentCrawlingService.startCrawling(userIds);
        String infoMessage = "Crawling started";
        LOG.info(infoMessage);
        return infoMessage;
    }

    @RequestMapping("/queue")
    public Collection<Integer> getQueue() {
        return commentCrawlingService.getUsersSet();
    }
}
