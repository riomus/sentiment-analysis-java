package pl.riomus.allegro.comment.crawler.clients;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import pl.riomus.allegro.client.AllegroAuthenticationData;
import pl.riomus.allegro.comment.AllegroDataProvider;

import javax.inject.Inject;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
@Configuration
@PropertySource("classpath:allegroCommentProvider.properties")
public class AllegroDataProviderConfiguration {
    private static final String ALLEGRO_USER_NAME_PROPERTY = "allegro.user.name";
    private static final String ALLEGRO_USER_PASSWORD_PROPERTY = "allegro.user.password";
    private static final String ALLEGRO_WEBAPI_KEY_PROPERTY = "allegro.webapi.key";
    private static final String ALLEGRO_COUNTRY_CODE_PROPERTY = "allegro.country.code";
    @Inject
    private Environment environment;

    @Bean
    public AllegroDataProvider createAllegroDataProvider() {
        return new AllegroDataProvider(createAllegroAuthenticationData());
    }

    private AllegroAuthenticationData createAllegroAuthenticationData() {
        String userLogin = System.getProperty(ALLEGRO_USER_NAME_PROPERTY, environment.getProperty(ALLEGRO_USER_NAME_PROPERTY));
        String userPassword = System.getProperty(ALLEGRO_USER_PASSWORD_PROPERTY, environment.getProperty(ALLEGRO_USER_PASSWORD_PROPERTY));
        String webapiKey = System.getProperty(ALLEGRO_WEBAPI_KEY_PROPERTY, environment.getProperty(ALLEGRO_WEBAPI_KEY_PROPERTY));
        String countryCode = System.getProperty(ALLEGRO_COUNTRY_CODE_PROPERTY, environment.getProperty(ALLEGRO_COUNTRY_CODE_PROPERTY));
        return new AllegroAuthenticationData(userLogin, userPassword, webapiKey, Integer.valueOf(countryCode));
    }
}
