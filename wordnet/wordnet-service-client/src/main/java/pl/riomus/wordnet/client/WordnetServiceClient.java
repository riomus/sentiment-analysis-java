package pl.riomus.wordnet.client;

import com.google.common.collect.Iterables;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.PagedResources;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import pl.riomus.spring.repository.client.PaginableRepositoryClient;
import pl.riomus.wordnet.api.LexicalUnitForSynsetProvider;
import pl.riomus.wordnet.api.models.LexicalUnit;

import java.util.Collection;
import java.util.Optional;
import java.util.logging.Logger;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
public class WordnetServiceClient extends PaginableRepositoryClient implements LexicalUnitForSynsetProvider {
    private static final Logger LOG = Logger.getLogger(WordnetServiceClient.class.getCanonicalName());
    private static final String ADDRESS_TEMPLATE = "%s/%s";
    public static final String SYNSET_ID_PARAMETER = "synsetId";
    public static final String LEMMA_PARAMETER = "lemma";
    private static final String GET_LEXICAL_UNITS_FOR_SYNSET = "lexicalUnit/search/findBySynset?synsetId={" + SYNSET_ID_PARAMETER + "}";
    private static final String GET_LEXICAL_UNITS_FOR_SYNSET_AND_LEMMA = "lexicalUnit/search/findBySynsetAndLemma?synsetId={" + SYNSET_ID_PARAMETER + "}&lemma={" + LEMMA_PARAMETER + "}";
    private final String getLexicalUnitsForSynsetFullAddress;
    private static final String GET_LEXICAL_UNITS = "lexicalUnit/search/findByLemma?lemma={" + LEMMA_PARAMETER + "}";
    private final String getLexicalUnitFullAddress;
    private final String getLexicalUnitsForSynsetAndLemmaFullAddress;

    public WordnetServiceClient(String repositoryAddress) {
        super(repositoryAddress);
        getLexicalUnitsForSynsetFullAddress = String.format(ADDRESS_TEMPLATE, repositoryAddress, GET_LEXICAL_UNITS_FOR_SYNSET);
        getLexicalUnitsForSynsetAndLemmaFullAddress = String.format(ADDRESS_TEMPLATE, repositoryAddress, GET_LEXICAL_UNITS_FOR_SYNSET_AND_LEMMA);
        getLexicalUnitFullAddress = String.format(ADDRESS_TEMPLATE, repositoryAddress, GET_LEXICAL_UNITS);
    }


    public Optional<Collection<LexicalUnit>> getLexicalUnitsForSynset(Long synsetId) {
        try {
            ResponseEntity<PagedResources<LexicalUnit>> serviceResponse =
                    restTemplate.exchange(getLexicalUnitsForSynsetFullAddress, HttpMethod.GET,
                            null, new ParameterizedTypeReference<PagedResources<LexicalUnit>>() {
                            }, synsetId);
            return Optional.of(serviceResponse.getBody().getContent());
        } catch (Exception e) {
            LOG.info("Failed to get lexical unit! " + e);
            return Optional.empty();
        }
    }


    public Optional<LexicalUnit> getLexicalUnitsForSynsetAndLemma(Long synsetId, String lemma) {
        try {
            ResponseEntity<PagedResources<LexicalUnit>> serviceResponse =
                    restTemplate.exchange(getLexicalUnitsForSynsetAndLemmaFullAddress, HttpMethod.GET,
                            null, new ParameterizedTypeReference<PagedResources<LexicalUnit>>() {
                            }, synsetId, lemma);
            Collection<LexicalUnit> content = serviceResponse.getBody().getContent();
            return Optional.of(Iterables.get(content, 0));
        } catch (Exception e) {
            LOG.info("Failed to get lexical unit! " + e);
            return Optional.empty();
        }
    }

    @Override
    public Optional<LexicalUnit> getLexicalUnit(String lemma) {
        try {
            ResponseEntity<PagedResources<LexicalUnit>> serviceResponse =
                    restTemplate.exchange(getLexicalUnitFullAddress, HttpMethod.GET,
                            null, new ParameterizedTypeReference<PagedResources<LexicalUnit>>() {
                            }, lemma);
            Collection<LexicalUnit> content = serviceResponse.getBody().getContent();
            return Optional.of(Iterables.get(content, 0));
        } catch (Exception e) {
            LOG.info("Failed to get lexical unit! " + e);
            return Optional.empty();
        }

    }

}
