package pl.riomus.wordnet.sentiment;

import com.google.common.collect.ImmutableMap;
import pl.riomus.wordnet.api.LexicalUnitSentimentProvider;
import pl.riomus.wordnet.api.models.LexicalUnit;
import pl.riomus.wordnet.api.models.Sentiment;

import java.util.Map;
import java.util.logging.Logger;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
public class BasicLexicalUnitSentimentProvider implements LexicalUnitSentimentProvider {
    private Map<String,Sentiment> ANNOTATION_TO_SENTIMENT= ImmutableMap.<String,Sentiment>builder().put("-", Sentiment.NEGATIVE)
            .put("+", Sentiment.POSITIVE)
            .put("0",Sentiment.NEUTRAL)
            .put("a",Sentiment.UNKNOWN).build();
    private static final Logger LOG=Logger.getLogger(BasicLexicalUnitSentimentProvider.class.getCanonicalName());
    @Override
    public Sentiment getSentiment(LexicalUnit lexicalUnit) {
        String comment = lexicalUnit.getComment();
        LOG.info("Extracting sentiment from comment "+comment);
        Sentiment out=Sentiment.UNKNOWN;
        String[] split = comment.split("##A");
        if(split.length>1){
               String sentimentComment=split[1];
            String[] extractedFeeling = sentimentComment.split("}");
            if(extractedFeeling.length==1){
                extractedFeeling=sentimentComment.split(":");
            }
            String sentimentCommentWithAnnotation = extractedFeeling[1];
            String sentimentAnnotationChar = sentimentCommentWithAnnotation.charAt(1)+"";
            out=ANNOTATION_TO_SENTIMENT.get(sentimentAnnotationChar);
        }
        return out==null?Sentiment.UNKNOWN:out;
    }

    public static void main(String[] args) {
        BasicLexicalUnitSentimentProvider basicLexicalUnitSentimentProvider = new BasicLexicalUnitSentimentProvider();
        LexicalUnit lexicalUnit = new LexicalUnit();
        lexicalUnit.setComment(" asdlfkgwlk ##k adsflkfd2 ##A {lol,lol} + m[asdgswfeq]");
        Sentiment sentiment = basicLexicalUnitSentimentProvider.getSentiment(lexicalUnit);
        System.out.println(sentiment);
    }
}
