package pl.riomus.wordnet.api;

import pl.riomus.wordnet.api.models.LexicalUnit;
import pl.riomus.wordnet.api.models.Sentiment;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
public interface LexicalUnitSentimentProvider {
    Sentiment getSentiment(LexicalUnit lexicalUnit);
}
