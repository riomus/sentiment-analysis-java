package pl.riomus.wordnet.api.models;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
public enum Sentiment {
    POSITIVE,NEGATIVE,NEUTRAL,UNKNOWN
}
