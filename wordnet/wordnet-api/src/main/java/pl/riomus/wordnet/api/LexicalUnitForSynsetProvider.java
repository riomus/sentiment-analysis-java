package pl.riomus.wordnet.api;

import pl.riomus.wordnet.api.models.LexicalUnit;

import java.util.Collection;
import java.util.Optional;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
public interface LexicalUnitForSynsetProvider {
    Optional<Collection<LexicalUnit>> getLexicalUnitsForSynset(Long synsetId);
    Optional<LexicalUnit> getLexicalUnitsForSynsetAndLemma(Long synsetId,String lemma);
    Optional<LexicalUnit> getLexicalUnit(String lemma);
}
