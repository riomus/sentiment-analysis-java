package pl.riomus.wordnet.configuration;

import org.springframework.context.annotation.Configuration;
import pl.riomus.spring.rest.coniguration.TransactionConfiguration;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
@Configuration
public class WordnetTransactionConfiguration extends TransactionConfiguration {
}
