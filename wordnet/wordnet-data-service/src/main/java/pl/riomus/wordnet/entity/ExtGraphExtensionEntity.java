package pl.riomus.wordnet.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigInteger;
import java.sql.Date;
import java.sql.Time;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
@Entity
@Table(name="extgraphextension")
public class ExtGraphExtensionEntity  {
    @Id
    @Column(name="EXTGRAPH_ID")
    private BigInteger extGraphId;
    @Column(name="REL_ID")
    private BigInteger relId;
    @Column(name="UNIT_ID")
    private BigInteger unitId;
    @Column(name="base")
    private Integer base;
    @Column(name="rank")
    private Double rank;

    public BigInteger getExtGraphId() {
        return extGraphId;
    }

    public void setExtGraphId(BigInteger extGraphId) {
        this.extGraphId = extGraphId;
    }

    public BigInteger getRelId() {
        return relId;
    }

    public void setRelId(BigInteger relId) {
        this.relId = relId;
    }

    public BigInteger getUnitId() {
        return unitId;
    }

    public void setUnitId(BigInteger unitId) {
        this.unitId = unitId;
    }

    public Integer getBase() {
        return base;
    }

    public void setBase(Integer base) {
        this.base = base;
    }

    public Double getRank() {
        return rank;
    }

    public void setRank(Double rank) {
        this.rank = rank;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ExtGraphExtensionEntity that = (ExtGraphExtensionEntity) o;

        if (extGraphId != null ? !extGraphId.equals(that.extGraphId) : that.extGraphId != null) return false;
        if (relId != null ? !relId.equals(that.relId) : that.relId != null) return false;
        if (unitId != null ? !unitId.equals(that.unitId) : that.unitId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = extGraphId != null ? extGraphId.hashCode() : 0;
        result = 31 * result + (relId != null ? relId.hashCode() : 0);
        result = 31 * result + (unitId != null ? unitId.hashCode() : 0);
        return result;
    }
}
