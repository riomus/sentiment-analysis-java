package pl.riomus.wordnet.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import pl.riomus.wordnet.entity.MessageEntity;
import pl.riomus.wordnet.entity.ParameterEntity;

import java.math.BigInteger;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
@RepositoryRestResource(collectionResourceRel = "parameter", path = "parameter")
public interface ParameterRepository extends PagingAndSortingRepository<ParameterEntity, BigInteger> {
}
