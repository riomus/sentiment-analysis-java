package pl.riomus.wordnet.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import pl.riomus.wordnet.entity.LexicalUnitEntity;

import java.math.BigInteger;
import java.util.List;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
@RepositoryRestResource(collectionResourceRel = "lexicalUnit", path = "lexicalUnit")
public interface LexicalUnitRepository extends PagingAndSortingRepository<LexicalUnitEntity, BigInteger> {

    @Query(value="SELECT * FROM lexicalunit WHERE lexicalunit.ID in (SELECT LEX_ID from unitandsynset WHERE SYN_ID= :synsetId )",nativeQuery = true)
    List<LexicalUnitEntity> findBySynset(@Param("synsetId")Long synsetId);

    @Query(value="SELECT * FROM lexicalunit WHERE lexicalunit.ID in (SELECT LEX_ID from unitandsynset WHERE SYN_ID= :synsetId ) AND lexicalunit.lemma=:lemma",nativeQuery = true)
    List<LexicalUnitEntity> findBySynsetAndLemma(@Param("synsetId")Long synsetId,@Param("lemma")String lemma);

    List<LexicalUnitEntity> findByLemma(@Param("lemma")String lemma);
}
