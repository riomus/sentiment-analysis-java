package pl.riomus.wordnet.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import pl.riomus.wordnet.entity.ExtGraphEntity;
import pl.riomus.wordnet.entity.LexicalUnitEntity;

import java.math.BigInteger;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
@RepositoryRestResource(collectionResourceRel = "extGraph", path = "extGraph")
public interface ExtGraphRepository extends PagingAndSortingRepository<ExtGraphEntity, BigInteger> {
}
