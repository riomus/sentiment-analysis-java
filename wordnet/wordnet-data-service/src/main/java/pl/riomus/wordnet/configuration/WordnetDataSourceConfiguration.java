package pl.riomus.wordnet.configuration;

import org.springframework.context.annotation.Configuration;
import pl.riomus.spring.rest.coniguration.DataSourceConfiguration;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
@Configuration
public class WordnetDataSourceConfiguration extends DataSourceConfiguration {
}
