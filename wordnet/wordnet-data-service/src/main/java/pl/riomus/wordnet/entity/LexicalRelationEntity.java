package pl.riomus.wordnet.entity;

import javax.persistence.*;
import java.math.BigInteger;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
@Entity
@Table(name = "lexicalrelation")
public class LexicalRelationEntity {
    @Id
    @Column(name = "CHILD_ID")
    private BigInteger childId;
    @Column(name = "PARENT_D")
    private BigInteger parentId;
    @Column(name = "REL_ID")
    private BigInteger relId;

    @Column(name = "owner")
    private String owner;

    @Column(name = "valid")
    private Integer valid;

    public BigInteger getChildId() {
        return childId;
    }

    public void setChildId(BigInteger childId) {
        this.childId = childId;
    }

    public BigInteger getParentId() {
        return parentId;
    }

    public void setParentId(BigInteger parentId) {
        this.parentId = parentId;
    }

    public BigInteger getRelId() {
        return relId;
    }

    public void setRelId(BigInteger relId) {
        this.relId = relId;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public Integer getValid() {
        return valid;
    }

    public void setValid(Integer valid) {
        this.valid = valid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LexicalRelationEntity that = (LexicalRelationEntity) o;

        if (childId != null ? !childId.equals(that.childId) : that.childId != null) return false;
        if (parentId != null ? !parentId.equals(that.parentId) : that.parentId != null) return false;
        if (relId != null ? !relId.equals(that.relId) : that.relId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = childId != null ? childId.hashCode() : 0;
        result = 31 * result + (parentId != null ? parentId.hashCode() : 0);
        result = 31 * result + (relId != null ? relId.hashCode() : 0);
        return result;
    }
}
