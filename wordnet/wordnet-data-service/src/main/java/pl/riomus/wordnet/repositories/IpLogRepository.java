package pl.riomus.wordnet.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import pl.riomus.wordnet.entity.IpLimitEntity;
import pl.riomus.wordnet.entity.IpLogEntity;

import java.math.BigInteger;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
@RepositoryRestResource(collectionResourceRel = "ipLog", path = "ipLog")
public interface IpLogRepository extends PagingAndSortingRepository<IpLogEntity, BigInteger> {
}
