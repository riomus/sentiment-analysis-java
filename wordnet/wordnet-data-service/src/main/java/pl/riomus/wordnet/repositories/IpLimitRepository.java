package pl.riomus.wordnet.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import pl.riomus.wordnet.entity.ExtGraphExtensionEntity;
import pl.riomus.wordnet.entity.IpLimitEntity;

import java.math.BigInteger;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
@RepositoryRestResource(collectionResourceRel = "ipLimit", path = "ipLimit")
public interface IpLimitRepository extends PagingAndSortingRepository<IpLimitEntity, BigInteger> {
}
