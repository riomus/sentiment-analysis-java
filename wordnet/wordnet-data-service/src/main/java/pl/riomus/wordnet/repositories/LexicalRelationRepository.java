package pl.riomus.wordnet.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import pl.riomus.wordnet.entity.LexicalRelationEntity;
import pl.riomus.wordnet.entity.LexicalUnitEntity;

import java.math.BigInteger;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
@RepositoryRestResource(collectionResourceRel = "lexicalRelation", path = "lexicalRelation")
public interface LexicalRelationRepository extends PagingAndSortingRepository<LexicalRelationEntity, BigInteger> {
}
