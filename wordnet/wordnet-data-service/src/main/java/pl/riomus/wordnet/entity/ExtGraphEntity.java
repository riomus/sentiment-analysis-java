package pl.riomus.wordnet.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigInteger;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
@Entity
@Table(name="extgraph")
public class ExtGraphEntity {
    @Id
    @Column(name="id")
    private BigInteger id;
    @Column(name="packageno")
    private BigInteger packageNo;
    @Column(name="pos")
    private Integer pos;
    @Column(name="score1")
    private Double score1;
    @Column(name="score2")
    private Double score2;
    @Column(name="synid")
    private BigInteger synId;
    @Column(name="weak")
    private Integer weak;
    @Column(name="word")
    private String word;

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public BigInteger getPackageNo() {
        return packageNo;
    }

    public void setPackageNo(BigInteger packageNo) {
        this.packageNo = packageNo;
    }

    public Integer getPos() {
        return pos;
    }

    public void setPos(Integer pos) {
        this.pos = pos;
    }

    public Double getScore1() {
        return score1;
    }

    public void setScore1(Double score1) {
        this.score1 = score1;
    }

    public Double getScore2() {
        return score2;
    }

    public void setScore2(Double score2) {
        this.score2 = score2;
    }

    public BigInteger getSynId() {
        return synId;
    }

    public void setSynId(BigInteger synId) {
        this.synId = synId;
    }

    public Integer getWeak() {
        return weak;
    }

    public void setWeak(Integer weak) {
        this.weak = weak;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ExtGraphEntity that = (ExtGraphEntity) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
