package pl.riomus.wordnet.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import pl.riomus.wordnet.entity.ExtGraphEvaluationEntity;
import pl.riomus.wordnet.entity.ExtGraphExtensionEntity;

import java.math.BigInteger;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
@RepositoryRestResource(collectionResourceRel = "extGraphExtension", path = "extGraphExtension")
public interface ExtGraphExtensionRepository extends PagingAndSortingRepository<ExtGraphExtensionEntity, BigInteger> {
}
