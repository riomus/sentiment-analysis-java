package pl.riomus.wordnet.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigInteger;
import java.sql.Date;
import java.sql.Time;

/**
 * Roman (Riomus) Bartusiak (riomus@gmail.com http://riomus.github.io)
 */
@Entity
@Table(name="extgraphevaluation")
public class ExtGraphEvaluationEntity {
    @Id
    @Column(name="id")
    private BigInteger id;
    @Column(name="assignedsynid")
    private BigInteger assignedSynId;
    @Column(name="comment")
    private String comment;
    @Column(name="date")
    private Date date;
    @Column(name="distance")
    private Integer distance;
    @Column(name="errorReasonTypeId")
    private BigInteger errorReasonTypeId;
    @Column(name="owner")
    private String owner;
    @Column(name="proposedconnectiontypeid")
    private BigInteger proposedConnectionTypeId;
    @Column(name="proposedsynid")
    private BigInteger proposedSynId;
    @Column(name="relationtypeid")
    private BigInteger relationTypeId;
    @Column(name="time")
    private Time time;
    @Column(name="word")
    private String word;

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public BigInteger getAssignedSynId() {
        return assignedSynId;
    }

    public void setAssignedSynId(BigInteger assignedSynId) {
        this.assignedSynId = assignedSynId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getDistance() {
        return distance;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    public BigInteger getErrorReasonTypeId() {
        return errorReasonTypeId;
    }

    public void setErrorReasonTypeId(BigInteger errorReasonTypeId) {
        this.errorReasonTypeId = errorReasonTypeId;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public BigInteger getProposedConnectionTypeId() {
        return proposedConnectionTypeId;
    }

    public void setProposedConnectionTypeId(BigInteger proposedConnectionTypeId) {
        this.proposedConnectionTypeId = proposedConnectionTypeId;
    }

    public BigInteger getProposedSynId() {
        return proposedSynId;
    }

    public void setProposedSynId(BigInteger proposedSynId) {
        this.proposedSynId = proposedSynId;
    }

    public BigInteger getRelationTypeId() {
        return relationTypeId;
    }

    public void setRelationTypeId(BigInteger relationTypeId) {
        this.relationTypeId = relationTypeId;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ExtGraphEvaluationEntity that = (ExtGraphEvaluationEntity) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
